<h1>PongGames</h1>
<p>PongGames is a peronal project to try out some modern JavaScript technologies, frameworks, libraries and features. The goal is to provide both a webpage offering several pong games for 2 to 4 players on mobile and desktop devices as well as a library, which makes programming new pong games easy and fast.</p>
<p>The GUI is implemented with React, so all GUI elements (see folder "Components") are ES6 React Component classes. The PongGame elements (that are drawn in the game canvas and stored in the folder "Elements"), however, are designed to work independently of any framework, enabling them to be used in "Non-React-applications", too.</p>
<p>As it is work in progress, there are still a lot of things to do ...</p>
<p>
<img src="/uploads/73008d06397f2487eecf16f12ca6d0db/2.png" alt="screenshowFirstPage"/>
</p>
<p>
<img src="/uploads/a1c470996b58df137d54647155e03a86/3.png" alt="screenshot2"/>
</p>
<p>
<img src="/uploads/7f704f3fe355eaade044ef6f79ccd992/Screenshot_2018-07-14_React_App.png" alt="screenshot1"/>
</p>
<p>
<img src="/uploads/8c727aeef493ed56182e7cc57c92fdaf/1.png" alt="screenshot3"/>
</p>