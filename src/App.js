import React, { Component } from 'react';
import { WelcomePage } from './Components/WelcomePage';
import { GameCollectionView } from './Components/GameCollectionView';
import { GameContainer } from './Components/GameContainer';
import { AnnounceWinnerPage } from './Components/AnnounceWinnerPage2';
import { getDefaultOptions } from './Games/GameCollection';
import { SettingsPage } from './Components/SettingComponents/SettingsPage';
import autoBind from 'auto-bind';
import './styles/App.scss';
import './styles/App-mobile.scss';

class App extends Component {

    constructor(props) {
        super(props);
        this.game = null;
        this.options = {};
        this.winners = [];
        this.losers = [];
        this.state = {
            selectedGame: 'none',
            step: 'start'
        };

        autoBind.react(this);
    }

    render() {
        if (this.state.step === 'start') {
        // if (false) {
            return (
              <div className="full-screen">
                <WelcomePage />
                <GameCollectionView onGameChosen={this.onGameChosen}/>
              </div>
            )
        } else if (this.state.step === 'settings' && this.state.selectedGame !== 'none') {
            // } else if (true) {
            return (
              <div className="full-screen">
                <SettingsPage language='en' chosenGame={this.state.selectedGame} onSettingsChanged={this.onSettingsChange}/>
                <GameCollectionView chosenGame={this.state.selectedGame} onGameChosen={this.onGameChosen} onGameStarted={this.onGameStarted}/>
              </div>
            )
        } else if (this.state.step === 'play') {
            return (
              <div className="full-screen">
                <GameContainer chosenGame={this.state.selectedGame} options={this.options[this.state.selectedGame]} onShowSettings={this.onShowSettings} onGameOver={this.onGameOver}/>
              </div>
            )
        } else if (this.state.step === 'over') {
            return (
              <div className="full-screen" onClick={this.reset}>
                <AnnounceWinnerPage winners={this.winners} losers={this.losers} onClick={()=>{
                    this.setState({ selectedGame: 'none' , gameStarted: false })
                }}/>
              </div>
            )
        }
    }

    onComponentDidMount () {
        debugger;
    }

    onGameChosen (gameId) {
        if (this.options[gameId] === undefined) {
            this.options[gameId] = getDefaultOptions(gameId)
        }
        this.setState({ step: 'settings', selectedGame: gameId });
    }

    onSettingsChange(path, value) {
        if (path.length === 1) {
            this.options[this.state.selectedGame][path[0]] = value;
        } else if (path.length === 2) {
            this.options[this.state.selectedGame][path[0]][path[1]] = value;
        } else if (path.length === 3) {
            this.options[this.state.selectedGame][path[0]][path[1]][path[2]] = value;
        } else if (path.length === 4) {
            this.options[this.state.selectedGame][path[0]][path[1]][path[2]][path[3]] = value;
        } else {
            console.error('Attribute path has not the expected number of elements.')
        }
    }

    onGameStarted() {
        this.setState({ step: 'play' });
    }

    /**
     * Functions to fire off the "WinnerAnnouncement"-Page
     * @param {Array} winners - array of strings with names of winners
     * @param {Array} losers - array of strings with names of losers
     */
    onGameOver(winners, losers) {
        this.winners = winners;
        this.losers = losers;
        this.setState({ step: 'over' });
    }

    onShowSettings() {
        this.setState({ step: 'settings' });
    }

    reset() {
        this.winners = [];
        this.losers = [];
        this.setState({ step: 'start' });
    }
}

export default App;