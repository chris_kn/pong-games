import { ColorTheme } from './Elements/ColorTheme'

export const colorThemes = [
    new ColorTheme('San Francisco Dance Hall 1947', ['1F1521','D8BF98', '917E63', 'B9564A', 'EB7364'], 1),
    new ColorTheme('Buoy', ['1B2B32', '37646F', 'E3A723', 'E1E7E8', 'BD322B'], 3),
    new ColorTheme('Spring Wedding', ['205038', '4A7028', 'E2C2C1', '813C51', '7A1326'], 2),
    new ColorTheme('Olympic Bold', ['588751', 'FFE9AC', 'D41942', 'FF6F25', 'FFC700'], 1),
    new ColorTheme('Molly Design', ['244D59', '98A639', 'B2A268', 'DE906D', 'FFFBF7'], 4),
    new ColorTheme('Palitra', ['0CABBE', '57D6D9', 'F0E2BA', 'F4965E', 'F0551C'], 2),
    new ColorTheme('SS18', ['A62E3A', 'BF696C', '10596B', '607549', 'E0DBC5'], 4)
];

export function getTheme(ID) {
    for (let i = colorThemes.length - 1; i >= 0; --i) {
        if (colorThemes[i].id === ID) {
            return colorThemes[i];
        }
    }
}

export function getRandomTheme() {
    return colorThemes[Math.floor(Math.random() * colorThemes.length)];
}