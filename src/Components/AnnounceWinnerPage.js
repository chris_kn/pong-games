import React, { Component } from 'C:/Users/C. Knörzer/AppData/Local/Microsoft/TypeScript/2.9/node_modules/@types/react';
import pongFigure1 from '../img/PongFigure1.svg';
// import '../styles/AnnounceWinnerPage.scss';

export class AnnounceWinnerPage extends Component {
    render() {

        let winners = [];
        for (let w = 0; w < this.props.winners.length; w++) {
            let icon = pongFigure1;

            if (w % 2 === 0) {
                icon.getElementsByClassName('mouth').style.display = 'none';
                icon.getElementById('smile2').style.display = 'inline';
            } else {
                icon.getElementsByClassName('mouth').style.display = 'none';
                icon.getElementById('smile1').style.display = 'inline';
            }

            // colorize
            icon.getElementsByClassName('colored-bar')[0].style.fill = this.props.winners[w].color;
            winners.push(<div className="figure" key={'winnerfigure' + w}>{icon}</div>);
        }

        let losers = [];
        for (let w = 0; w < this.props.losers.length; w++) {
            let icon = pongFigure1;

            if (w % 2 === 0) {
                icon.getElementsByClassName('mouth').style.display = 'none';
                icon.getElementById('sad2').style.display = 'inline';
            } else {
                icon.getElementsByClassName('mouth').style.display = 'none';
                icon.getElementById('sad1').style.display = 'inline';
            }

            // colorize
            icon.getElementsByClassName('colored-bar')[0].style.fill = this.props.losers[w].color;
            losers.push(<div className="figure" key={'loserfigure' + w}>{icon}</div>);
        }

        let figureContainer;

        if (this.props.winners.position === 'left') {
            figureContainer = <div class="figures-container">
                                    <div className="figures-left-team">{winners}</div>
                                    <div className="figures-right-team">{losers}</div>
                                </div>;
        } else {
            figureContainer = <div class="figures-container">
                                    <div className="figures-left-team">{losers}</div>
                                    <div className="figures-right-team">{winners}</div>
                                </div>;
        }

        let announcement = this.props.winners[0].name;

        if (this.props.winners.length > 1) {
            for (let i = 1; i < this.props.winners.length; ++i) {
                announcement = announcement + ' and ' + this.props.winners[i].name;
            }

            announcement = announcement + ' win the game!'
        } else {
            announcement = announcement + ' wins the game!'
        }

        return (
            <div className="full-screen">
                <div class="winner-announcement">
                    {announcement}
                </div>
                {figureContainer}
            </div>
        )
    }
}