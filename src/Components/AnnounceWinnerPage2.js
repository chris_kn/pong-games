import React, { Component } from 'react';
import WebFont from 'webfontloader';
import '../styles/AnnounceWinnerPage.scss';

export class AnnounceWinnerPage extends Component {
    constructor() {
        super();
        WebFont.load({
            google: {
                families: ['Droid Sans', 'Droid Serif']
            }
        });
    }

    render() {

        const winners = this.props.winners;
        const losers = this.props.losers;
        let announcement;

        if (winners.length === 1 && losers.length < 1) {
            announcement = 'Congratulations, you won!';
        } else if (losers.length > 0 && winners.length < 1) {
            announcement = 'Uh, sorry dude, you lost.'
        } else if (losers.length < 1 && winners.length > 1) {
            announcement = "Everybody wins! ... or ... maybe ... it's a draw.";
        } else if (winners.length > 0){

            // take name of winner, if not set, take label and add to string
            if (winners[0].name !== undefined && winners[0].name !== '') {
                announcement = winners[0].name;
            } else {
                announcement = winners[0].label;
            }

            if (winners.length > 1) {
                for (let i = 1; i < winners.length; ++i) {
                    // take name of winner, if not set, take label and add to string
                    if (winners[i].name !== undefined && winners[i].name !== '') {
                        announcement = announcement + ' and ' + winners[i].name;
                    } else {
                        announcement = announcement + ' and ' + winners[i].label;
                    }
                    
                }

                announcement = announcement + ' win the game!';
            } else {
                announcement = announcement + ' wins the game!';
            }
        } else {
            announcement = 'Game over!';
        }

        return (
            <div className="full-screen">
                <div className="winner-announcement-container">
                    <div className="winner-announcement">
                        {announcement}
                    </div>             
                </div>
            </div>
        )
    }
}