import React, { Component } from 'react';
import autoBind from 'auto-bind';

export class ColorThemePanel extends Component {

    constructor(props) {
        super(props);
        this.colors = this.props.colorTheme.copyAllColors();
        this.squares = [];

        for(let i = 0 ; i < this.colors.length; ++i) {
            let bg = { backgroundColor: this.colors[i] };
            this.squares.push(<div key={'color_' + this.colors[i]} className="color-square" style={bg}></div>);
        }

        autoBind.react(this);
    }

    render() {
        return(
            <div className='color-theme-panel'>
                {this.squares}
            </div>
        )
    }
}