import React, { Component } from 'react';
import { colorThemes } from '../ColorCollection';
import { ColorThemePanel } from './ColorThemePanel';
import { Swipable } from './Swipable';
import previousIcon from '../img/arrow-left.svg';
import nextIcon from '../img/arrow-right.svg';
import autoBind from 'auto-bind';
import '../styles/ColorThemePicker2.scss';

export class ColorThemePicker extends Component {

    constructor(props) {
        super(props);
        this.colorThemes = colorThemes;
        this.themePanels = [];
        this.scrollPanel = React.createRef();
        autoBind.react(this);
        this.startIndex = this.colorThemes.length % 2 === 0 ? (this.colorThemes.length-2)/2 : (this.colorThemes.length-1)/2;
        this.lastIndex = null;
        this.selectedIndex = this.startIndex;
        this.posX = 0;
        this.oldPosX = 0;
        this.themePanels = [];

        this.themePanels = this.colorThemes.map((item, i) => {
            return (<ColorThemePanel key={this.colorThemes[i].name} colorTheme={this.colorThemes[i]}/>);
        });
    }

    componentDidMount() {
        this.updatePosX();
    }

    componentWillUpdate() {
        //this.updatePosX();
    }

    updatePosX() {
        this.oldPosX = this.posX;
        
        const scrollNode = this.scrollPanel.current;
        const selectedNode = scrollNode.childNodes[this.selectedIndex];
        
        const paddingLeft = Math.round((scrollNode.parentNode.clientWidth - selectedNode.clientWidth) / 2);
        this.posX = paddingLeft - selectedNode.offsetLeft;

        scrollNode.style.transform = 'translate('+ this.posX + 'px)';

        if (this.lastIndex !== null) {
            scrollNode.childNodes[this.lastIndex].classList.remove('selected');
        }
        scrollNode.childNodes[this.selectedIndex].classList.add('selected');
    }

    nextButtonPressed() {
        this.lastIndex = this.selectedIndex;
        this.selectedIndex = this.selectedIndex + 1 < this.colorThemes.length ? this.selectedIndex + 1 : 0;
        this.updatePosX();
        this.handleSelection();
        
    }

    previousButtonPressed() {
        this.lastIndex = this.selectedIndex;
        this.selectedIndex = this.selectedIndex - 1 > -1 ? this.selectedIndex - 1 : this.colorThemes.length - 1;
        this.updatePosX();
        this.handleSelection();
        
    }

    render() {
        
        return(
            <div className="color-picker-container">
                <div className="color-picker-navigation color-picker-previous-button" onClick={this.previousButtonPressed}><img src={previousIcon} alt="previous"/></div>  
                <Swipable onSwipeLeft={this.nextButtonPressed} onSwipeRight={this.previousButtonPressed} className="color-picker-scroll-container">  
                    <div className="color-picker-scroll-panel" ref={this.scrollPanel}>
                        {this.themePanels}
                    </div>
                </Swipable>
                <div className="color-picker-navigation color-picker-next-button" onClick={this.nextButtonPressed}><img src={nextIcon} alt="next"/></div>
            </div>
        )
    }

    handleSelection() {

        this.props.onSelection(['colorTheme'], this.colorThemes[this.selectedIndex].id);
    }

}