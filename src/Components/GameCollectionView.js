import React, { Component } from 'react';
import { GameCollection } from '../Games/GameCollection';
import autoBind from 'auto-bind';
import startIcon from '../img/play-overlay.svg';
import '../styles/GameCollectionView.scss';

export class GameCollectionView extends Component {

    constructor(props) {
        super(props);
        this.gameItems = [];  
        autoBind.react(this);
    }

    render() {
        this.createIcons();
        return (
            <div className="game-collection">
                {this.gameItems}
            </div>
        );
    }

    createIcons() {
        this.gameItems = GameCollection.map((game, index) => {
            if (game.id === this.props.chosenGame) {
                // display selected game with play overlay
                return (
                    <div key={'game' + index} className="game-icon selected" onClick={() => {this.props.onGameStarted(game.id)}}>
                        <img src={startIcon} alt="play-overlay" className="game-icon-overlay"/>
                        <img src={game.icon} alt={game.name}/>
                        
                    </div>
                )
            } else if (game.notPublished === true) {
                // return inactive game icon as game is still being developped
                return(
                    <div key={'game' + index} className="game-icon">
                        <div className="game-icon-overlay inactive-overlay">Coming soon!</div>
                        <img src={game.icon} alt={game.name}/>
                        
                    </div> 
                )
            } else {
                // display normal icon
                return(
                    <div key={'game' + index} className="game-icon" onClick={() => {this.props.onGameChosen(game.id)}}>
                        <img src={game.icon} alt={game.name}/>
                        <img src={startIcon} alt="play-overlay" className="game-icon-overlay invisible"/>
                    </div> 
                )
            } 
        });
    }

}