import React, { Component } from 'react';
import { getGame } from '../Games/GameCollection'
import autoBind from 'auto-bind';
import { PlayerControls } from './PlayerControls';
import PlayIcon from '../img/play-arrow.svg';
import SettingsIcon from '../img/settings.svg';
import '../styles/GameContainer.scss';

export class GameContainer extends Component {

    constructor(props) {

        super(props);
        autoBind.react(this);
        this.id = "game-container";
        let Game = getGame(this.props.chosenGame);
        this.onScreenControls = this.setOnScreenControls();

        let callbacks = {
            onPause: this.pause,
            onUnPause: this.unpause,
            onGameOver: this.onGameOver
        };
        
        this.game = new Game(this.id, this.props.options, callbacks);
        this.state = { paused: false };
        
    }

    render() {
        
        let overLayClassName = this.state.paused ? 'pause-overlay full-screen' : 'pause-overlay invisible';
        const invisiblePauseButtonClassName = this.state.paused ? 'invisible-pause-button invisible' : 'invisible-pause-button';
        let controlsLeftClassName = this.onScreenControls[0].length > 0 ? 'on-screen-controls' : 'on-screen-controls invisible';
        let controlsRightClassName = this.onScreenControls[1].length > 0 ? 'on-screen-controls' : 'on-screen-controls invisible';

        return(
            <div className="full-screen">
                <div className="full-screen game-wrapper">
                    <div className={controlsLeftClassName}>{this.onScreenControls[0]}</div>
                    <div className="canvas-container">
                        <canvas id={this.id}/>
                        <div className={invisiblePauseButtonClassName} onClick={()=>{
                            this.pause();
                            this.game.pause();
                        }}></div>
                    </div>
                    <div className={controlsRightClassName}>{this.onScreenControls[1]}</div>
                </div>
                <div className={overLayClassName}>
                    <div className="show-settings-button" onClick={this.props.onShowSettings}>
                        <img src={SettingsIcon} alt="Settings" />
                    </div>
                    <div className="continue-game-button" onClick={() => {
                            this.unpause();
                            this.game.unpause();
                    }}>
                        <img src={PlayIcon} alt="Continue" />
                    </div>
                </div>
            </div>
        ) 
        
    }

    pause() {  
        this.setState({ paused: true });
    }

    unpause() {
        this.setState({ paused: false });
    }

    onGameOver(winners, losers) {
        this.props.onGameOver(winners, losers);
    }

    componentDidMount() {
        this.setState({ paused: false });
        this.game.setup();
    }

    componentWillUnmount() {
        this.game.clear();
        this.game.callbacks = {};
        this.game = null;
    }

    setOnScreenControls() {
        let controls = [[],[]];
        const players = this.props.options.players;

        for (let p = 0; p < players.length; ++p) {
            var currPlayer = players[p];
            if (currPlayer.activated && !currPlayer.autopilot && !currPlayer.useKeyboard) {
                if (p % 2 === 0) {
                    controls[0].push(<PlayerControls game={this.props.chosenGame} index={p} key={'PlayerControl' + p} controls={players[p].controls} handleTouchStart={this.handleTouchStart} handleTouchEnd={this.handleTouchEnd}/>);
                } else {
                    controls[1].push(<PlayerControls game={this.props.chosenGame} index={p} key={'PlayerControl' + p} controls={players[p].controls} handleTouchStart={this.handleTouchStart} handleTouchEnd={this.handleTouchEnd}/>);
                }
            }
        }

       return controls;
    }

    handleTouchStart(playerIndex, key) {
        this.game.handleKeyDown({
            key: 'OnScreenControl' + playerIndex + key,
            preventDefault: function(){}
        });
    }

    handleTouchEnd(playerIndex, key) {
        this.game.handleKeyUp({
            key: 'OnScreenControl' + playerIndex + key,
            preventDefault: function(){}
        });
    }

}