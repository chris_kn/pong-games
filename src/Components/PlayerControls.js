import React, { Component } from 'react';
import { getGameDescription } from '../Games/GameCollection';

export class PlayerControls extends Component {

    constructor(props) {
        super(props);
        this.controlsDescription = getGameDescription(this.props.game).controls;
    }

    render() {

        this.controlsArray = [];
        let keys = Object.keys(this.props.controls);

        for (let i = 0; i < keys.length; ++i) {
            const className = 'control-button ' + keys[i];

            this.controlsArray.push(
                <div className={className} key={'control_player_' + this.props.index + '_' + keys[i]}
                    onMouseDown={(event) => {
                        event.preventDefault();
                        this.props.handleTouchStart(this.props.index, keys[i]);
                    }}
                    onMouseOut={(event) => {
                        event.preventDefault();
                        this.props.handleTouchEnd(this.props.index, keys[i]);
                    }}
                    onMouseUp={(event) => {
                        event.preventDefault();
                        this.props.handleTouchEnd(this.props.index, keys[i]);
                    }}
                    onTouchStart={(event) => {
                        event.preventDefault();
                        this.props.handleTouchStart(this.props.index, keys[i]);
                    }}
                    onTouchEnd={(event) => {
                        event.preventDefault();
                        this.props.handleTouchEnd(this.props.index, keys[i]);
                    }}
                    onTouchCancel={(event) => {
                        event.preventDefault();
                        this.props.handleTouchEnd(this.props.index, keys[i]);
                    }}
                    //TODO: find an equivalent to mouseOut for touch
                    /* onTouchMove={(event) => {
                        event.preventDefault();
                        var touch = event.touches[0];
                        if (that !== document.elementFromPoint(touch.pageX,touch.pageY)) {
                            that.props.handleTouchEnd(that.props.index, keys[i]);
                        }
                    }} */
                    >
                        <img src={this.controlsDescription[keys[i]].icon} alt={keys[i]}/>
                </div>
            );
        }

        return (

            <div className="player-controls">
                {this.controlsArray}
            </div>

        );
    }

}