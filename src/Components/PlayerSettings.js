// import React, { Component } from 'react';
// import { ToggleButton } from './ToggleButton';
// import { TextInput } from './TextInput';
// import { SetControlInput } from './SetControlInput';

// export default class PlayerSettings extends Component {

//     constructor(props) {
//         super(props);
//         this.controlKeys = Object.keys(this.props.defaultOptions.controls);
//         this.currKeyNames = this.props.defaultOptions.controls;
//         this.state = { 
//             useOnScreenControls: this.props.defaultOptions.useOnScreenControls,
//             autopilot: this.props.defaultOptions.autopilot
//         };

//         // Setting grid of elements
//         this.settingLines = [];
//         const numberLines = 4;
//         for (let i = 0; i < numberLines; i++) {
//             this.settingLines.push([]);
//         }

//         const that = this;
//         this.firstLine = [];
//         this.secondLine = [];

//         this.settingLines[0].push(<div key='settings_player_label' className="player-settings-label">Player {this.props.index + 1}</div>);

//         if (this.props.disable === 'true') {
//             this.settingLines[0].push(<div key={'settings_enable'} className="toggle-button-container"><ToggleButton enabled={this.props.defaultOptions.enabled} handleChange={(enabled) => {
//                 that.props.onPlayerSettingsChanged(['players', this.props.index, 'enabled'], enabled);
//             }}/></div>);
//         } else {
//             this.settingLines[0].push(<div key={'settings_enable'} className="toggle-button-container"></div>);
//         }

//         if (this.props.index === 1) {
//             this.settingLines[0].push(<div key="auto-pilot-label" className="auto-pilot-label">autopilot</div>);
//             this.settingLines[0].push(<div key={'auto-pilot'} className="toggle-button-container"><ToggleButton enabled={this.state.autopilot} handleChange={(enabled) => {
//                 this.props.onPlayerSettingsChanged(['players', this.props.index, 'autopilot'], enabled);
//             }}/></div>)
//         }

//         this.settingLines[1].push(<div key="text_input-label" className="text-input-label">name</div>);
//         this.settingLines[1].push(<TextInput key={'settings_name'} handleChange={(event) => {
//             that.props.onPlayerSettingsChanged(['players', this.props.index, 'name'], event.target.value);
//         }}/>);

//         // thirdLine.push(<div key='settings_toggle_use_onscreen' className="player-settings-label">use onscreen buttons</div>);

//         // thirdLine.push(<div key={'settings_useOnScreenControls'} className="toggle-button-container"><ToggleButton enabled={this.props.defaultOptions.useOnScreenControls} label="use on-screen controls" handleChange={(enabled) => {
//         //     this.setUseOnScreenControls(enabled);
//         // }}/></div>);

//         this.settings.push(<div key='settings_firstLine' className="settings-line">{this.firstLine}</div>);
//         this.settings.push(<div key='settings_secondLine' className="settings-line">{this.secondLine}</div>);
//         this.settings.push(<div></div>);
//     }

//     render() {
//         this.settingLines[2].push(<div key={'settings_useOnScreenControls'} className="toggle-button-container"><ToggleButton activated={!this.state.autopilot} enabled={this.props.defaultOptions.useOnScreenControls} label="use on-screen controls" handleChange={(enabled) => {
//             this.setUseOnScreenControls(enabled);
//         }}/></div>);
//         this.settingLines[3].push(<div key='settings_thirdLine' className="settings-line">{thirdLine}</div>);
//         const that = this;
//         let controlLines = [];
//         const controlButtonsActivated = !this.state.useOnScreenControls && !this.state.autopilot;
//         let keyButtonClassName = controlButtonsActivated ? 'set-control-input activated' : 'set-control-input';

//         for (let i = 0; i < this.controlKeys.length; ++i) { // take dependecy of className away, do that in the componant with prop
//             this.settingLines[4].push(<SetControlInput activated={controlButtonsActivated} className={keyButtonClassName} key={'settings_controls_' + i} defaultKey={this.currKeyNames[this.controlKeys[i]]} label={this.props.keyLabels[this.controlKeys[i]].name} handleChange={(keyName) => {
//                 that.changeControlButton(this.controlKeys[i], keyName);
//             }}/>);
//         }

//         this.settings.splice(this.settings.length - 1, 1, <div key='settings_controlLines' className="settings-control-lines">{controlLines}</div>);

//         return (
//             <div className="player-settings-panel">
//                 {this.settingLines}

//             </div>
//         )
//     }

//     setUseOnScreenControls (bool) {
//         this.props.onPlayerSettingsChanged(['players', this.props.index, 'useOnScreenControls'], bool);
//         this.setState({ useOnScreenControls: bool });
//     }

//     changeControlButton (directionName, key) {
//         if (!this.state.useOnScreenControls) {
//             this.currKeyNames[directionName] = key;
//             this.props.onPlayerSettingsChanged(['players', this.props.index, 'controls', directionName, key]);
//         }
//     }

//     setUseAutoPilot (bool) {
//         this.props.onPlayerSettingsChanged(['players', this.props.index, 'autopilot'], bool);
//         this.setState({ autopilot: bool });
//     }

// }