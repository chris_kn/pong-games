import React, { Component } from 'react';
import ToggleButton from '../ToggleButton';

export default class PlayerEnableButton extends Component {
    render () {
        return (
            <div className="player-enable-field toggle-container">
                <ToggleButton activated={this.props.activated} handleChange={this.handleChange.bind(this)}/>
            </div>
        );
    }

    handleChange (activated) {
        this.props.handleChange(['players', this.props.index, 'activated'], activated);
    }
}