import React, { Component } from 'react';
import SetControlInput from './SetControlInput';

export default class PlayerKeySettings extends Component {
    render () {
        let buttons = [];
        const keyNames = Object.keys(this.props.controls);

        for (let k = 0; k < keyNames.length; k++) {
            const label = this.props.controlDescription[keyNames[k]].name;
            const defaultKey = this.props.controls[keyNames[k]];
            buttons.push(<SetControlInput 
                key={"key-control-input" + k} 
                enabled={this.props.enabled} 
                defaultKey={defaultKey} 
                label={label}
                handleChange={this.props.handleChange}
            />);
        }

        return (
            <div className="player-key-settings">
                {buttons}
            </div>
        );
    }
}