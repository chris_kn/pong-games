import React, { Component } from 'react';
import get from '../../Translations';

export default class PlayerLabel extends Component {
    render () {
        return (
            <div className="player-label">
                {get('en', 'player') + ' ' + (this.props.index + 1)}
            </div>
        );
    }
}