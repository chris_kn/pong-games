import React, { Component } from 'react';
import TextInput from '../TextInput';
import get from '../../Translations';

export default class PlayerNameInput extends Component {

    render () {
        return (
            <div className="player-name-input">
                <TextInput onChange={this.handleChange} label={get(this.props.language, 'name')} />
            </div>
        );
    }

    handleChange (e) {
        this.props.handleChange(['players', this.props.index, 'name'], e.target.value);
    }
}