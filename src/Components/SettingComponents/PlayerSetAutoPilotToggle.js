import React, { Component } from 'react';
import ToggleButton from '../ToggleButton';
import get from '../../Translations';

export default class PlayerSetAutoPilotToggle extends Component {
    render () {
        return (
            <div className="toggle-container set-autopilot-toggle">
                <ToggleButton 
                    label={get(this.props.language, 'autopilot')}
                    activated={this.props.activated} 
                    handleChange={this.handleChange.bind(this)}
                />
            </div>
        );
    }

    handleChange (activated) {
        this.props.handleChange(['players', this.props.index, 'autopilot'], activated);
    }
}