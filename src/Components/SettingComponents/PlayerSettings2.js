import React, { Component } from 'react';
import PlayerLabel from './PlayerLabel';
import PlayerEnableToggle from './PlayerEnableToggle';
import PlayerNameInput from './PlayerNameInput';
import PlayerUseKeyboardToggle from './PlayerUseKeyboardToggle';
import PlayerSetAutoPilotToggle from './PlayerSetAutoPilotToggle';
import PlayerKeySettings from './PlayerKeySettings';
import '../../styles/PlayerSettings2.scss';

export default class PlayerSettings2 extends Component {
    constructor (props) {
        super(props);
        this.state = {
            usingKeyboard: this.props.useKeyboard,
            usingAutopilot: this.props.autopilot,
            keySettingsEnabled: this.props.useKeyboard,
            wasUsingKeyboard: this.props.useKeyboard
        };
    }

    render () {
        this.rows = [];

        // first row
        if (this.props.enableToggle) {
            this.rows.push(this.wrapInRow([
                <PlayerLabel 
                    index={this.props.index} 
                    key={'player-label-' + this.props.index}
                />,
                <PlayerEnableToggle
                    index={this.props.index}
                    handleChange={this.props.handleChange} 
                    activated={this.props.activated} 
                    key={'player-enable-toggle-' + this.props.index}
                />
            ]));
        } else if (this.props.autoPilotToggle) {
            this.rows.push(this.wrapInRow([
                <PlayerLabel 
                    index={this.props.index} 
                    key={'player-label-' + this.props.index}
                />,
                <PlayerSetAutoPilotToggle
                    index={this.props.index}
                    language={this.props.language}
                    handleChange={this.toggleUseAutopilot.bind(this)} 
                    activated={this.props.autopilot} 
                    key={'player-set-autopilot-toggle-' + this.props.index}
                />
            ]));
        } else {
            this.rows.push(this.wrapInRow(<PlayerLabel 
                index={this.props.index} 
                key={'player-label-' + this.props.index}
            />));
        }

        // second row
        this.rows.push(this.wrapInRow(<PlayerNameInput
            index={this.props.index}
            language={this.props.language}
            currName={this.props.name} 
            enabled={this.props.enabled && !this.props.autopilot} 
            handleChange={this.props.handleChange}
            key={'player-name-input-' + this.props.index}
        />));
        
        // third row
        this.rows.push(this.wrapInRow(<PlayerUseKeyboardToggle 
            language={this.props.language}
            index={this.props.index}
            activated={this.state.usingKeyboard}
            enabled={!this.state.usingAutopilot}
            handleChange={this.toggleUseKeyboards.bind(this)}
            key={'player-use-keyboard-toggle-' + this.props.index}
        />));

        // fourth & fifth row (key settings)
        this.rows.push(<PlayerKeySettings
            index={this.props.index}
            enabled={this.state.keySettingsEnabled} 
            controls={this.props.controls} 
            controlDescription={this.props.controlDescription}
            handleChange={this.props.handleChange}
            key={'player-key-settings-' + this.props.index}
        />);
        return (
            <div className="player-settings">
                {this.rows}
            </div>
        );
    }

    toggleUseAutopilot (path, enabled) {
        var newState = {};
        newState.usingAutopilot = enabled;
        
        if (enabled && this.state.usingKeyboard) {
            newState.wasUsingKeyboard = true;
            newState.usingKeyboard = false;
            newState.keySettingsEnabled = false;
        } else if (!enabled && this.state.wasUsingKeyboard) {
            newState.usingKeyboard = true;
            newState.keySettingsEnabled = true;
            newState.wasUsingKeyboard = false;
        }

        this.props.handleChange(path, enabled);
        this.setState(newState);
    }

    toggleUseKeyboards (path, enabled) {
        this.props.handleChange(path, enabled);
        this.setState({ usingKeyboard: enabled, keySettingsEnabled: enabled });
    }

    wrapInRow(content, customClassName) {
        return (
            <div key={'player-settings-row-' + this.rows.length} className={customClassName ? customClassName : 'settings-row'}>
                {content}
            </div>
        );
    }
}