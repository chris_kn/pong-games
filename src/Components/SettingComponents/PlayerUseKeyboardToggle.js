import React, { Component } from 'react';
import ToggleButton from '../ToggleButton';
import get from '../../Translations';

export default class PlayerUseKeyboardToggle extends Component {
    render () {
        return (
            <div className="toggle-container use-keyboard-toggle">
                <ToggleButton 
                    label={get(this.props.language, 'useKeyboard')} 
                    activated={this.props.activated}
                    enabled={this.props.enabled}
                    handleChange={this.handleChange.bind(this)}
                />
            </div>
        );
    }

    handleChange (enabled) {
        this.props.handleChange(['players', this.props.index, 'useKeyboard'], enabled);
    }
}