import React, { Component } from 'react';
import autoBind from 'auto-bind';
import '../../styles/SetControlInput.scss';

export default class SetControlInput extends Component {

    constructor(props) {
        super(props);
        this.state = {
            receiving: false,
            keyName: this.props.defaultKey,
            // enabled: this.props.enabled !== false
        }
        this.forbiddenKeys = [' ', 'control'];
        this.myButton = React.createRef();
        autoBind.react(this);
    }

    render() {
        const enabled = this.props.enabled ? ' enabled' : '';
        const receiving = this.state.receiving ? ' receiving' : '';
        const className = 'set-control-input' + enabled + receiving;
        const label = this.state.receiving ? 'press key' : this.props.label + ': ' + this.state.keyName;
        return(
            <button ref={this.myButton} className={className} onClick={this.handleClick} onKeyDown={this.keypressed}>
                {label}
            </button>
        )
    }

    handleClick() {
        if (this.props.enabled) {
            if (this.state.receiving === true) {
                this.setState({ receiving: false });
            } else {
                this.setState({ receiving: true });
            }
        }
    }

    keypressed(e) {
        if(this.state.receiving) {
            if (this.forbiddenKeys.indexOf(e.key) === -1) {
                this.props.handleChange(e.key);
                this.setState({ receiving: false, keyName: e.key });
            } else {
                console.error('Key not allowed as control!');
            }
        }
    }
}