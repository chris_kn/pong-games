import React, { Component } from 'react';
import { getDefaultOptions, getGameDescription } from '../../Games/GameCollection';
import PlayerSettings2 from './PlayerSettings2';
import { ColorThemePicker } from '../ColorThemePicker2';
import '../../styles/SettingsPage.scss';

export class SettingsPage extends Component {

    constructor(props) {

        super(props);
        const desc = getGameDescription(this.props.chosenGame);
        const defaultOpts = getDefaultOptions(this.props.chosenGame);
        
        this.playerSettings = [];
        let autoPilotToggle = false;

        for (let i = 0; i < desc.minPlayers; ++i) {
            if (i === 1) {
                autoPilotToggle = true;
            }
            const options = defaultOpts.players[i];

            this.playerSettings.push(<PlayerSettings2 
                key = {'player-settings_' + i} 
                index = {i} 
                handleChange = {this.props.onSettingsChanged} 
                enableToggle = {false}
                language = {this.props.language}
                enabled = {options.enabled}
                controls = {options.controls}
                autopilot = {options.autopilot}
                autoPilotToggle = {autoPilotToggle}
                name = {options.name}
                useKeyboard = {options.useKeyboard}
                controlDescription = {desc.controls}
            /> );
        }
        for (let i = desc.minPlayers; i < desc.maxPlayers; ++i) {
            const options = defaultOpts.players[i];

            this.playerSettings.push(<PlayerSettings2 
                key = {'player-settings_' + i} 
                index = {i} 
                handleChange = {this.props.onSettingsChanged} 
                enableToggle = {true}
                language = {this.props.language}
                enabled = {options.enabled}
                controls = {options.controls}
                autoPilotToggle = {autoPilotToggle}
                name = {options.name}
                controlDescription = {desc.controls}
            />);
        } 
    }

    render() {
        return(
            <div className="main-panel settings-panel">
                {this.playerSettings}
                <ColorThemePicker className="color-theme-picker" onSelection={this.props.onSettingsChanged}/>
            </div>
        );
    }
}