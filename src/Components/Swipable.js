import React, { Component } from 'react';
import autoBind from 'auto-bind';

export class Swipable extends Component {
    constructor(props) {
        super(props);
        this.props = props;
        this.callbacks = {};
        this.callbacks.onSwipeLeft = this.props.onSwipeLeft || function(){};
        this.callbacks.onSwipeRight = this.props.onSwipeRight || function(){}; 
        this.callbacks.onSwipeUp = this.props.onSwipeUp || function(){}; 
        this.callbacks.onSwipeDown = this.props.onSwipeDown || function(){};
        this.mouseDown = false;
        this.startX = undefined;
        this.startY = undefined;
        this.threshold = props.threshold || 200;
        this.maxTime = 500;
        autoBind.react(this);
    }

    onSwipeLeft(callback) {
        this.callbacks.onSwipeLeft = callback;
    }

    onSwipeRight(callback) {
        this.callbacks.onSwipeRight = callback;
    }

    onSwipeUp(callback) {
        this.callbacks.onSwipeUp = callback;
    }

    onSwipeDown(callback) {
        this.callbacks.onSwipeDown = callback;
    }

    render() {
        return (
        <div className={"swipable " + this.props.className} onMouseDown={this.handleMouseDown} onMouseMove={this.handleMouseMove} onMouseUp={this.handleMouseUp} onTouchStart={this.handleTouchStart} onTouchMove={this.handleTouchMove} onTouchEnd={this.handleTouchEnd}>
            {this.props.children}
        </div>
        )
    }

    handleMouseDown(event) {
        this.mouseDown = true;
        this.startX = event.pageX;
        this.startY = event.pageY;
        this.startTime = new Date().getTime(); // record time when finger first makes contact with surface
        event.preventDefault();
    }

    handleMouseMove(event) {
        if (!this.mouseDown) return;
        if (this.startX === undefined || this.startY === undefined || this.startTime === undefined) return;
        if (new Date().getTime() - this.startTime > this.maxTime) return;

        const distX = event.pageX - this.startX;
        const distY = event.pageY - this.startY;

        if (Math.abs(distX) > this.threshold && Math.abs(distX) > Math.abs(distY)) {
            if (distX > 0) {
                this.callbacks.onSwipeRight();
            } else if (distX < 0) {
                this.callbacks.onSwipeLeft();
            }
            this.handleMouseUp();
        } else if (Math.abs(distY) > this.threshold && Math.abs(distY) > Math.abs(distX)) {
            if (distY > 0) {
                this.callbacks.onSwipeDown()
            } else if (distY < 0) {
                this.callbacks.onSwipeUp();
            }
            this.handleMouseUp();
        }
        event.preventDefault();
        
    }

    handleMouseUp(event) {
        this.mouseDown = false;
        this.startX = undefined;
        this.startY = undefined;
        this.startTime = undefined;
        if(event !== undefined) event.preventDefault();
    }

    handleTouchStart(event) {
        this.startX = event.changedTouches[0].pageX;
        this.startY = event.changedTouches[0].pageY;
        this.startTime = new Date().getTime(); // record time when finger first makes contact with surface
        event.preventDefault();
    }

    handleTouchMove(event) {

        if (this.startX === undefined || this.startY === undefined || this.startTime === undefined) return;
        if (new Date().getTime() - this.startTime > this.maxTime) return;

        const distX = event.changedTouches[0].pageX - this.startX;
        const distY = event.changedTouches[0].pageY - this.startY;

        if (Math.abs(distX) > this.threshold && Math.abs(distX) > Math.abs(distY)) {
            if (distX > 0) {
                this.callbacks.onSwipeRight();
            } else if (distX < 0) {
                this.callbacks.onSwipeLeft();
            }
            this.handleTouchEnd();
        } else if (Math.abs(distY) > this.threshold && Math.abs(distY) > Math.abs(distX)) {
            if (distY > 0) {
                this.callbacks.onSwipeDown()
            } else if (distY < 0) {
                this.callbacks.onSwipeUp();
            }
            this.handleTouchEnd();
        }
        event.preventDefault();
    }

    handleTouchEnd(event) {
        this.startX = undefined;
        this.startY = undefined;
        this.startTime = undefined;
        if(event !== undefined) event.preventDefault();
    }
}