import React, { Component } from 'react';
import '../styles/TextInput.scss';

export default class TextInput extends Component {

    render() {
        return (
            <div className="text-input">
                <label>{this.props.label}</label>
                <input onChange={this.props.handleChange}/>
            </div>
                    );
    }
}