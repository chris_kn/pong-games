import React, { Component } from 'react';
import autoBind from 'auto-bind';
import '../styles/ToggleButton.scss';

export default class ToggleButton extends Component {

    constructor(props) {
        super(props);
        this.state = {
            focused: false,
            activated: this.props.activated === true
        };
        autoBind.react(this);
    }

    render() {
        let className = this.state.activated ? 'switch active toggle-button' : 'switch toggle-button';
        if (this.props.enabled === false) {
            className += ' disabled';
        }
        return (
            <div className="toggle-input">
                <label>{this.props.label}</label>
                <div className={className} onFocus={this.handleFocus} onClick={this.handleClick}>
                    <div className="slider" ></div>
                </div>
            </div>
        );
    }

    handleClick() {
        if (this.props.enabled !== false) {
            if (this.state.activated === true) {
                this.props.handleChange(false);
                this.setState({ activated: false });
            } else {
                this.props.handleChange(true);
                this.setState({ activated: true });
            }
        }
    }

    handleFocus(e) {
        this.setState({ focused: e.focused });
    }

}