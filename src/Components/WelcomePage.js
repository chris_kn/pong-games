import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { World } from '../Elements/World';
import { Ball } from '../Elements/Ball';
import { Bar } from '../Elements/Bar';
import { Writing } from '../Elements/Writing';
import { colorThemes } from '../ColorCollection';
import autoBind from 'auto-bind';
import { Wall } from '../Elements/Wall';
import '../styles/WelcomePage.scss';

export class WelcomePage extends Component {

    constructor() {
        super();

        this.colorTheme = colorThemes[Math.floor(Math.random() * colorThemes.length)];
        this.canvasId = "welcome-canvas";
        this.canvas = React.createRef();
        this.mainPanel = React.createRef();
        autoBind.react(this);
    }

    setup() {
        
        let canvas = ReactDOM.findDOMNode(this.canvas.current);
        this.c = canvas.getContext('2d');
        this.world = new World({});
        this.frameStep = 1 / 60 * 1000;
        
        canvas.style.background = this.colorTheme.bg();
        this.world.addElement(new Wall(this.c, -0.02, 0.5, 0.02, 1, 'black'));
        this.world.addElement(new Wall(this.c, 1.02, 0.5, 0.02, 1, 'black'));
        this.world.addElement(new Wall(this.c, 0.5, -0.02, 1, 0.02, 'black'));
        this.world.addElement(new Wall(this.c, 0.5, 1.02, 1, 0.02, 'black'));
        const bar1 = new Bar({
            world: this.world, 
            context: this.c,
            playerIndex: 0, 
            teamIndex: 0, 
            options: {}, 
            x: 0.5, 
            y: 0.25, 
            width: 0.3, 
            height: 0.14, 
            color: this.colorTheme.getRandomUnique()
        });
        const bar2 = new Bar({
            world: this.world, 
            context: this.c, 
            playerIndex: 0, 
            teamIndex: 1, 
            options: {},
            x: 0.5,
            y: 0.75,
            width: 0.3,
            height: 0.14,
            color: this.colorTheme.getRandomUnique()
        });
        this.world.addElement(bar1);
        this.world.addElement(bar2);
        this.ball = new Ball({
            world: this.world,
            c: this.c,
            x: 0.5,
            y:  0.5,
            radius: 0.03,
            color: this.colorTheme.getRandomUnique()
        });
        this.world.addElement(this.ball);

        this.world.addElement(new Writing(this.c, 'Welcome', bar1.position.x, bar1.position.y, bar1.height - 0.04, 'Droid Sans', 'white'));
        this.to2 = new Writing(this.c, '2', this.ball.position.x, this.ball.position.y, this.ball.height - 0.02, 'Droid Sans', 'white');
        this.world.addElement(this.to2);
        this.world.addElement(new Writing(this.c, 'PongGames', bar2.position.x, bar2.position.y, bar2.height - 0.04, 'Droid Sans', 'white'));

        let oldTime = Date.now();
        
        this.canvasResize();
        this.canvasRender(oldTime);
        
        this.world.balls[0].kickoff(.004);
    }

    componentDidMount() {
        this.setup();
        // this.canvasResize();
        window.addEventListener("resize", this.canvasResize);
    }

    componentWillUnmount() {
        window.cancelAnimationFrame(this.frameId);
        window.removeEventListener("resize", this.canvasResize);
    }

    canvasRender(oldTime) {
        let newTime = Date.now();
        let delta = newTime - oldTime;

        if (delta >= this.frameStep) {

            this.c.clearRect(0, 0, this.canvas.current.width, this.canvas.current.height);
            

            this.world.update();
            this.world.draw();

            this.to2.setPosition(this.ball.position);
            oldTime = newTime - (delta % this.frameStep);
        }


        this.frameId = window.requestAnimationFrame(this.canvasRender, oldTime);
    }

    canvasResize() {
        // const mainPanel = ReactDOM.findDOMNode(this.mainPanel.current);
        const mainPanel = this.mainPanel.current;

        this.canvas.current.height = mainPanel.clientHeight;
        this.canvas.current.width = mainPanel.clientWidth; 
        // this.canvas.current.height = this.div.clientHeight;
        // this.canvas.current.width = this.div.clientWidth;
    }

    render() {
        return (
            <div className="main-panel" /* ref={div => { this.div = div; }} */ ref={this.mainPanel} >
                <canvas ref={ this.canvas } id={this.canvasId}></canvas>
            </div>
        );
    }

}
