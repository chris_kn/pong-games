import { lightenDarkenColor, generateID } from '../Tools.js';
import autoBind from 'auto-bind';

export class Ball {
    /**
     * Ball element that moves within the World and collides with other elements in the same
     * World element.
     * @param {World} world 
     * @param {CanvasRenderingContext2D} c 
     * @param {number} x - 
     * @param {number} y - start position of ball relative to canvas height
     * @param {number} radius - factor of canvas diagonal to determine ball radius
     * @param {string} color - color of the ball
     */
    constructor({ world, c, x = 0.5 , y = 0.5 , radius = 0.03 , color = 'white' , startingSpeed , speedAugmentation , minDimensionalSpeed , callbacks }) {
        this.className = 'Ball';
        this.isStatic = false;
        this.id = generateID();
        this.c = c;
        this.world = world;
        this.radius = radius;
        this.color = color;
        this.mass = 10 + Math.random() * 3;
        this.visible = true;
        this.minDimensionalSpeed = minDimensionalSpeed;
        this.minSpeed = startingSpeed;
        this.startingSpeed = startingSpeed;
        this.speedAugmentation = speedAugmentation;
        callbacks = callbacks || {};
        this.onCollisionCallback = callbacks.onCollision;

        this.position = {
            x: x,
            y: y
        };
        this.velocity = {
            x: 0,
            y: 0
        };
        this.nextVelocity = {
            x: null,
            y: null
        };

        this.absSizeX = null;
        this.absSizeY = null;
        this.pausing = false;
        this.checkCounter = 0;
        this.kickedOff = false;

        autoBind(this);
    }

    /**
     * Sets the radius of the ball in respect to the canvas diagonal
     * @param {number} newSize - factor of canvas diagonal, usually a small value between 0.01 and 0.1
     */
    setSize(newSize) {

        if (newSize <= 0) {
            console.error('Size of Ball cannot be zero or negative.');
            return
        }
        if (newSize > 1) {
            console.warn('Ball should not be bigger than canvas! Check the size!');
        }
        this.radius = newSize;
    }

    /**
     * Draws the element on the canvas.
     */
    draw() {

        if (!this.visible) return;

        this.c.beginPath();
        this.c.arc(this.getAbsX(this.position.x), this.getAbsY(this.position.y), this._getAbsDiagonal(this.radius), 0, 2 * Math.PI, false);
        this.c.fillStyle = lightenDarkenColor(this.color, 40);
        this.c.fill();
        this.c.lineWidth = 1;
        this.c.strokeStyle = this.color;
        this.c.stroke();

        if (this.disease !== undefined) {
            this.c.strokeStyle = '#FF0000';
            this.c.setLineDash([10, 5]);
            this.c.stroke();
            this.c.setLineDash([]);
        }

        this.checkCounter++;

        // check regularly if element is too slow
        if (this.checkCounter > 50) {
            this.checkCounter = 0;
            this.checkSpeed();
        }
    }

    /**
     * Updates velocity values if element is about to collide with another element
     */
    checkCollisions() {
        // loop through balls to check for collisions
        for (let i = 0; i < this.world.balls.length; ++i) {
            let b = this.world.balls[i];
            if (b.id !== this.id) {
                const distance = Math.sqrt(Math.pow(this.getAbsX((this.position.x + this.velocity.x) - (b.position.x + b.velocity.x)), 2) + Math.pow(this.getAbsY((this.position.y + this.velocity.y) - (b.position.y + b.velocity.y)), 2));
                if (distance < this._getAbsDiagonal(this.radius + b.radius)) {
                    this.nextVelocity.x = (this.mass * this.velocity.x + b.mass * (2 * b.velocity.x - this.velocity.x)) / (this.mass + b.mass);
                    this.nextVelocity.y = (this.mass * this.velocity.y + b.mass * (2 * b.velocity.y - this.velocity.y)) / (this.mass + b.mass);
                }
            }
        }

        // loop through bars to check for collisions
        for (let i = 0; i < this.world.players.length; ++i) {
            this._checkCollision_ball_rect(this.world.players[i]);
        }

        // loop through walls to check for collisions
        for (let i = 0; i < this.world.walls.length; ++i) {
            this._checkCollision_ball_rect(this.world.walls[i]);
        }
    }

    /**
     * Applies velocity values to the element's position
     */
    update() {
        // if a new velocity value was calculated due to a detected collision, update velocity first
        if (this.nextVelocity.x !== null) {
            this.velocity.x = this.nextVelocity.x;
            this.nextVelocity.x = null;
        }
        if (this.nextVelocity.y !== null) {
            this.velocity.y = this.nextVelocity.y;
            this.nextVelocity.y = null;
        }

        this.position.x = this.position.x + this.velocity.x;
        this.position.y = this.position.y + this.velocity.y;

        // apply friction if not zero
        if (typeof this.world.friction === 'number') {
            this._applyFriction(this.world.friction);
        }

        // update velocity if gravity is not zero
        if (typeof this.world.gravity === 'number') {
            this.velocity.y += this.world.gravity;
        }
    }

    /**
     * Starts the movement of the Ball in a random direction.
     * @param {number} force - Number that influeces the speed
     * @param {number} delay - In ms, delay before kickoff is triggered. (optional, default 0) 
     */
    kickoff(force = this.startingSpeed || 2, delay = 0) {
        // force = force || this.startingSpeed || 2;
        this.kickedOff = false;
        this.kickoffForce = force;
        this.checkCounter = 0;
        this.minSpeed = force;
        let signX = Math.random() >= 0.5 ? 1 : -1;
        let signY = Math.random() >= 0.5 ? 1 : -1;
        let angle = Math.random(0.3 * Math.PI);

        let velX = signX * this.minSpeed * Math.cos(angle);
        let velY = signY * this.minSpeed * Math.sin(angle);

        setTimeout(() => {
           this.setVelocity(velX, velY);
           this.kickedOff = true;
        }, delay * 1000);
    }

    /**
     * Mulitplies the current velocity by the input factor
     * @param {number} factor 
     */
    changeSpeed(factor) {
        if (typeof factor === 'number') {
            this.velocity.x = this.velocity.x * factor;
            this.velocity.y = this.velocity.y * factor;
        }
    }

    /**
     * Sets the elements velocity, that is the current speed in each dimension
     * @param {number} x 
     * @param {number} y 
     */
    setVelocity(x, y) {
        if (typeof x === 'number' && typeof y === 'number') {
            this.velocity.x = x;
            this.velocity.y = y;
        } else {
            console.error('Method "setVelocity" only accepts numbers!');
        }
    }

    /**
     * Method returns the absolute value of a value that is relative
     * to the canvas' width
     * @param {number} x - Relative number value
     */
    getAbsX(x) {
        return x * this.c.canvas.width;
    }

    /**
     * Method returns the absolute value of a value that is relative
     * to the canvas' height
     */
    getAbsY(y) {
        return y * this.c.canvas.height;
    }

    /**
     * Method returns the next positions and border positions IN PIXELS after the next 
     * potential movement of the Ball
     */
    getNextStepAbs() {
        return {
            radius: this._getAbsDiagonal(this.radius),
            x: this.getAbsX(this.position.x + this.velocity.x),
            y: this.getAbsY(this.position.y + this.velocity.y),
            topBorder: this.getAbsY(this.position.y) - this._getAbsDiagonal(this.radius) + this.getAbsY(this.velocity.y),
            bottomBorder: this.getAbsY(this.position.y) + this._getAbsDiagonal(this.radius) + this.getAbsY(this.velocity.y),
            leftBorder: this.getAbsX(this.position.x + this.velocity.x) - this._getAbsDiagonal(this.radius),
            rightBorder: this.getAbsX(this.position.x + this.velocity.x) + this._getAbsDiagonal(this.radius)
        }
    }

    pause() {
        this.pausing = true;
    }

    unpause() {
        this.pausing = false;
    }

    /**
     * Method returns the lenght of the velocity vector
     */
    getSpeed (speedX = this.velocity.x, speedY = this.velocity.y) {
        return Math.sqrt(Math.pow(speedX, 2) + Math.pow(speedY, 2));
    }

    checkSpeed () {
        if (this.kickedOff === true && this.minDimensionalSpeed) {
            const xFactor = Math.abs(this.minDimensionalSpeed.x) / Math.abs(this.velocity.x);
            const yFactor = Math.abs(this.minDimensionalSpeed.y) / Math.abs(this.velocity.y);
            if (xFactor > 1 || yFactor > 1) {
                this.flash();
                this.kickoffForce = this.kickoffForce || 6;
                this.kickoff(this.kickoffForce, 2000);
            }
        }
    }

    getPositionX() {
        return this.position.x;
    }

    getPositionY() {
        return this.position.y;
    }

    flash() {
        this.kickedOff = false;
        this.visible = false;
        setTimeout(() => {
            this.visible = true;
            setTimeout(() => {
                this.visible = false;
                setTimeout(() => {
                    this.visible = true;
                }, 500);
            }, 500);
        }, 500);
    }

    _applyFriction (amount) {
        const reducer = 1 - amount;
        if (this.getSpeed(this.velocity.x * reducer, this.velocity.y * reducer) >= this.minSpeed) {
            this.changeSpeed(reducer);
        }
    }

    /**
     * Internal method calculating and resolving collisions of this ball and a rectangle passed to the method
     * @param {Bar} rect 
     */
    _checkCollision_ball_rect(rect) {
        const bm = this.getNextStepAbs();
        const rm = rect.getNextStepAbs();
        let collision = false;
        // check if Ball hits edge of rectangle
        if (Math.abs(bm.x - rm.x) < bm.radius + rm.width / 2 && bm.y <= rm.bottomBorder && bm.y >= rm.topBorder) {
            if (Math.sign(this.velocity.x) === Math.sign(rect.velocity.x) && this.velocity.x < rect.velocity.x) {
                this.nextVelocity.x = rect.velocity.x;
            } else {
                this.nextVelocity.x = -this.velocity.x + rect.velocity.x / 10;
            }
            collision = true;
        } else if (Math.abs(bm.y - rm.y) < bm.radius + rm.height / 2 && bm.x <= rm.rightBorder && bm.x >= rm.leftBorder) {
            if (Math.sign(this.velocity.y) === Math.sign(rect.velocity.y) && this.velocity.y < rect.velocity.y) {
                this.nextVelocity.y = rect.velocity.y;
            } else {
                this.nextVelocity.y = -this.velocity.y + rect.velocity.y / 10;
            }
            collision = true;
        }
        // check if Ball hits corner point
        else if (Math.sqrt(Math.pow(rm.topBorder - bm.y, 2) + Math.pow(rm.leftBorder - bm.x, 2)) <= bm.radius) {
            // collision with left top border

            // include impulse response 
            let speed = this._cheapImpulseResponse_ball_rect(rect);

            let xComp = Math.abs(rm.leftBorder - bm.x);
            let yComp = Math.abs(rm.topBorder - bm.y);
            this.nextVelocity.x = -speed * Math.sin(Math.atan(xComp / yComp));
            this.nextVelocity.y = -speed * Math.cos(Math.atan(xComp / yComp));
            collision = true;
        } else if (Math.sqrt(Math.pow(rm.topBorder - bm.y, 2) + Math.pow(rm.rightBorder - bm.x, 2)) <= bm.radius) {
            // collision with top right border
            // include impulse response 
            let speed = this._cheapImpulseResponse_ball_rect(rect);
            let xComp = Math.abs(rm.rightBorder - bm.x);
            let yComp = Math.abs(rm.topBorder - bm.y);
            this.nextVelocity.x = speed * Math.sin(Math.atan(xComp / yComp));
            this.nextVelocity.y = -speed * Math.cos(Math.atan(xComp / yComp));
            collision = true;
        } else if (Math.sqrt(Math.pow(rm.bottomBorder - bm.y, 2) + Math.pow(rm.leftBorder - bm.x, 2)) <= bm.radius) {
            // collision with bottom left border
            // include impulse response 
            let speed = this._cheapImpulseResponse_ball_rect(rect);
            let xComp = Math.abs(rm.leftBorder - bm.x);
            let yComp = Math.abs(rm.bottomBorder - bm.y);
            this.nextVelocity.x = -speed * Math.sin(Math.atan(xComp / yComp));
            this.nextVelocity.y = speed * Math.cos(Math.atan(xComp / yComp));
            collision = true;
        } else if (Math.sqrt(Math.pow(rm.bottomBorder - bm.y, 2) + Math.pow(rm.rightBorder - bm.x, 2)) <= bm.radius) {
            // collision with bottom right border
            // include impulse response 
            let speed = this._cheapImpulseResponse_ball_rect(rect);
            let xComp = Math.abs(rm.rightBorder - bm.x);
            let yComp = Math.abs(rm.bottomBorder - bm.y);
            this.nextVelocity.x = speed * Math.sin(Math.atan(xComp / yComp));
            this.nextVelocity.y = speed * Math.cos(Math.atan(xComp / yComp));
            collision = true;
        }

        // if set, call callback (e.g. can be used to accelerate the ball on collision)
        if (collision === true) {
            if (typeof this.speedAugmentation === 'number') {
                // increase the minimal / base speed (raises friction "limit")
                this.minSpeed = this.minSpeed * (1 + this.speedAugmentation);
                // increase actual current speed
                this.changeSpeed(1 + this.speedAugmentation);
            }
            if (typeof this.onCollisionCallback === 'function') {
                this.onCollisionCallback(this, rect);
            }
        }
    }

    /**
     * Simulates a cheap calculation of an impulse response. If signs are inverse for one
     * dimension, the velocity is added to the ball
     * @param {Bar} rect 
     */
    _cheapImpulseResponse_ball_rect(rect) {
        let speed;

        // cheap impulse simulation
        if (rect.velocity === undefined) {
            speed = this.getSpeed();
        } else if (Math.sign(this.velocity.x) === -Math.sign(rect.velocity.x)) {
            speed = this.getSpeed() + Math.abs(rect.velocity.x);
        } else if (Math.sign(this.velocity.y) === -Math.sign(rect.velocity.y)) {
            speed = this.getSpeed() + Math.abs(rect.velocity.y);
        } else {
            speed = this.getSpeed();
        }

        return speed;
    }

    setCollisionCallback(onCollision) {
        if (typeof onCollision === 'function') {
            this.onCollisionCallback = onCollision;
        }
    }

    /**
     * Returns the absolute value for the input value which is relative to the canvas diagonal.
     * Used internally to calculate the ball radius.
     * @param {number} factor - number >= 0, usually a small value close to 0.
     */
    _getAbsDiagonal(factor) {
        let diagonal = Math.sqrt(Math.pow(this.c.canvas.height, 2) + Math.pow(this.c.canvas.width, 2));
        return diagonal * factor;
    }

    /**
     * Returns the ball's radius as an absolute pixel value
     */
    getAbsRadius() {
        return this._getAbsDiagonal(this.radius);
    }
}