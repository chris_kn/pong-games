import { lightenDarkenColor, generateID } from '../Tools';
import autoBind from 'auto-bind';

export class Bar {
    /**
     * A rectangle element that can be controlled by the user (e.g. to prevent
     * Balls from leaving the screen on "their side" of the canvas)
     * @param {World} world 
     * @param {CanvasRenderingContext2D} context 
     * @param {number} playerIndex 
     * @param {number} teamIndex 
     * @param {object} options 
     * @param {number} x 
     * @param {number} y 
     * @param {number} width 
     * @param {number} height 
     * @param {String} color 
     * @param {number} speed 
     */
    constructor({ world , context , playerIndex , teamIndex , options , x = 0.5 , y = 0.5 , width = 0.015 , height = 0.1 , color = 'white' , speed = 0.02 }) {
        this.className = 'Bar';
        this.isStatic = false;
        this.id = generateID();
        this.c = context;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.position = {
            x: x,
            y: y
        };
        this.velocity = {
            x: 0,
            y: 0
        };
        this.nextVelocity = {
            x: null,
            y: null
        }
        this.speed = {
            x: speed,
            y: speed
        };

        this.color = color;
        this.controls = options.controls;

        if (options.useKeyboard === false) {
            this.controls = {
                up: 'OnScreenControl' + playerIndex + 'up',
                down: 'OnScreenControl' + playerIndex + 'down',
                left: 'OnScreenControl' + playerIndex + 'left',
                right: 'OnScreenControl' + playerIndex + 'right',
            }
        }

        this.name = options.name;
        this.label = 'Player' + playerIndex;
        this.teamIndex = teamIndex;
        this.absSizeX = null;
        this.absSizeY = null;
        this.moveUp = false;
        this.moveDown = false;
        this.moveLeft = false;
        this.moveRight = false;
        this.world = world;

        autoBind(this);

    }

    draw() {

        this.c.beginPath();
        this.c.rect(this.getAbsX(this.leftBorder()), this.getAbsY(this.topBorder()), this.getAbsX(this.width), this.getAbsY(this.height));

        //console.log(lightenDarkenColor(this.color, 40));
        this.c.fillStyle = lightenDarkenColor(this.color, 40);
        this.c.strokeStyle = this.color;
        this.c.lineWidth = 1;
        this.c.fill();
        this.c.stroke();

        if (this.disease !== undefined) {
            this.c.strokeStyle = '#FF0000';
            if (this.disease.name !== 'transparency') {
                this.c.setLineDash([10, 5]);
                this.c.stroke();
                this.c.setLineDash([]);
            }
        }
    }

    /**
     * Method returns the absolute width (in pixels) for the relative width fraction input
     * @param {number} fraction - Fraction of the canvas width
     */
    getAbsX(fraction) {
        return Math.round(this.c.canvas.width * fraction);
    }

    /**
     * Method returns the absolute height (in pixels) for the relative height fraction input
     * @param {number} fraction 
     */
    getAbsY(fraction) {
        return Math.round(this.c.canvas.height * fraction);
    }

    getVerticalSpeed() {
        return this.velocity.x * this.c.canvas.height / 500;
    }

    getHorizontalSpeed() {
        return this.velocity.y * this.c.canvas.width / 1000;
    }

    setSpeed(value) {
        if (value >= 0) {
            this.speed = value;
        } else {
            console.error('Invalid speed value! Only values of 0 or bigger are valid.')
        }

    }

    stopMove() {
        this.moveUp = false;
        this.moveDown = false;
        this.moveLeft = false;
        this.moveRight = false;
    }

    checkCollisions() {

        // if velocity is zero there either won't be a collision or it is handled by the moving element
        if (this.velocity.x === 0 && this.velocity.y === 0) return;

        // loop through bars to check for collisions
        for (let i = 0; i < this.world.players.length; ++i) {
            if (this.world.players[i].id !== this.id) {
                let b = this.world.players[i];
                if (this.velocity.x !== 0) {
                    if (Math.abs(this.position.x + this.velocity.x - (b.position.x + b.velocity.x)) < (this.width + b.width) / 2 &&
                        this.topBorder() < b.bottomBorder() && this.bottomBorder() > b.topBorder()) {
                        this.nextVelocity.x = 0;
                    }
                }
                if (this.velocity.y !== 0) {
                    if (Math.abs(this.position.y + this.velocity.y - (b.position.y + b.velocity.y)) < (this.height + b.height) / 2 &&
                        this.leftBorder() < b.rightBorder() && this.rightBorder() > b.leftBorder()) {
                        this.nextVelocity.y = 0;
                    }
                }
            }
        }

        // loop through walls to check for collisions
        for (let i = 0; i < this.world.walls.length; ++i) {
            let w = this.world.walls[i];
            if (this.velocity.x !== 0) {
                if (Math.abs(this.position.x + this.velocity.x - w.position.x) < (this.width + w.width) / 2) {
                    this.nextVelocity.x = 0;
                }
            }
            if (this.velocity.y !== 0) {
                if (Math.abs(this.position.y + this.velocity.y - w.position.y) < (this.height + w.height) / 2) {
                    this.nextVelocity.y = 0;
                }
            }
        }
    }

    update() {
        this._updateVelocityOnColision();
        this._updatePosition();
        this._updateVelocityOnUserInput();
    }

    _updateVelocityOnColision () {
        // update velocity if collision requests adaption
        if (this.nextVelocity.x !== null) {
            this.velocity.x = this.nextVelocity.x;
            this.nextVelocity.x = null;
        }
        if (this.nextVelocity.y !== null) {
            this.velocity.y = this.nextVelocity.y;
            this.nextVelocity.y = null;
        }
    }

    _updatePosition () {
        this.position.x += this.velocity.x;
        this.position.y += this.velocity.y;
    }

    _updateVelocityOnUserInput () {
        // update velocity upon user input for next cycle
        let x = 0;
        let y = 0;

        if (this.moveUp === true) {
            y -= this.speed.y;
        }
        if (this.moveDown === true) {
            y += this.speed.y;
        }
        if (this.moveLeft === true) {
            x -= this.speed.x;
        }
        if (this.moveRight === true) {
            x += this.speed.x;
        }

        this.velocity.x = x;
        this.velocity.y = y;
    }

    onEvent(eventName, e = 0) {
        if (eventName === 'keydown') {
            if (e.key === this.controls.up) {
                this.moveUp = true;
            } else if (e.key === this.controls.down) {
                this.moveDown = true;
            } else if (e.key === this.controls.left) {
                this.moveLeft = true;
            } else if (e.key === this.controls.right) {
                this.moveRight = true;
            }
        } else if (eventName === 'keyup') {
            if (e.key === this.controls.up) {
                this.moveUp = false;
            } else if (e.key === this.controls.down) {
                this.moveDown = false;
            } else if (e.key === this.controls.left) {
                this.moveLeft = false;
            } else if (e.key === this.controls.right) {
                this.moveRight = false;
            }
        } else if (eventName === 'pausing') {
            this.pause();
        } else if (eventName === 'unpausing') {
            this.unpause();
        }
    }

    rightBorder() {
        return this.position.x + 0.5 * this.width;
    }

    leftBorder() {
        return this.position.x - 0.5 * this.width;
    }

    topBorder() {
        return this.position.y - 0.5 * this.height;
    }

    bottomBorder() {
        return this.position.y + 0.5 * this.height;
    }

    getNextStep() {
        return {
            x: this.x + this.velocity.x,
            y: this.y + this.velocity.y,
            topBorder: this.topBorder() + this.velocity.y,
            bottomBorder: this.bottomBorder() + this.velocity.y,
            leftBorder: this.leftBorder() + this.velocity.x,
            rightBorder: this.rightBorder() + this.velocity.x
        }
    }

    getNextStepAbs() {
        return {
            width: this.getAbsX(this.width),
            height: this.getAbsY(this.height),
            x: this.getAbsX(this.position.x + this.velocity.x),
            y: this.getAbsY(this.position.y + this.velocity.y),
            topBorder: this.getAbsY(this.topBorder() + this.velocity.y),
            bottomBorder: this.getAbsY(this.bottomBorder() + this.velocity.y),
            leftBorder: this.getAbsX(this.leftBorder() + this.velocity.x),
            rightBorder: this.getAbsX(this.rightBorder() + this.velocity.x)
        }
    }

    pause() {
        this.pausing = true;
    }
    unpause() {
        this.pausing = false;
    }

}