import { Bar } from './Bar';

export class BarAutoPilot extends Bar {
    constructor (options) {
        super(options);
        if (options.autoDimension !== 'horizontal' && options.autoDimension !== 'vertical') {
            throw new Error('options.autoDimension must be either "horizontal" or "vertical"');
        }
        this.autoDimension = options.autoDimension;
        this.frameDelay = Math.round(options.delay * this.world.frameStep);
        this.frameCount = 0;
        this.currentGoal = {};
    }

    _updateVelocityOnUserInput () {
        // user is computer, calculate movement from ball position
        if (!this.world.balls[0]) return;
        if (this.frameCount >= this.frameDelay) {
            this.frameCount = 0;
            this._calculateNextTargetPoint();
            // if (this.autoDimension === 'vertical') {
            //     this.currentGoal.y = this._findClosestBall().position.y;
            // } else if (this.autoDimension === 'horizontal') {
            //     this.currentGoal.x = this._findClosestBall().position.x;
            // }
        }

        if (this.currentGoal.x > this.rightBorder()) {
            this.moveLeft = false;
            this.moveRight = true;
        } else if (this.currentGoal.x < this.leftBorder()) {
            this.moveRight = false;
            this.moveLeft = true;
        } else {
            this.moveRight = false;
            this.moveLeft = false;
        }

        if (this.currentGoal.y > this.bottomBorder()) {
            this.moveUp = false;
            this.moveDown = true;
        } else if (this.currentGoal.y < this.topBorder()) {
            this.moveDown = false;
            this.moveUp = true;
        } else {
            this.moveDown = false;
            this.moveUp = false;
        }

        this.frameCount++;
        super._updateVelocityOnUserInput();
    }

    _findClosestBall (aD) {
        let currDistance;
        let closestBall;

        for (let b = 0; b < this.world.balls.length; b++) {
            const ball = this.world.balls[b];
            if (this._isBallMovingTowardsBar(ball, aD)) {
                const distance = Math.abs(ball.position[aD] - this.position[aD]);
                if (distance < currDistance || currDistance === undefined) {
                    closestBall = ball;
                    currDistance = distance;
                }
            }
        } 
    
        return closestBall;
    }

    /**
     * @param {Ball} ball The ball in question
     * @param {string} sD staticDimension, either 'x' or 'y', the dimension the Bar
     * is not supposed to move in automatically
     */
    _isBallMovingTowardsBar (ball, sD) {
        if ((ball.position[sD] > this.position[sD] && ball.velocity[sD] < 0)
            || (ball.position[sD] < this.position[sD] && ball.velocity[sD] > 0)) {
            return true;
        } else {
            return false;
        }
    }

    _calculateNextTargetPoint () {
        if (this.autoDimension === 'vertical') {
            this.currentGoal.y = this._estimateHitPoint(this._findClosestBall('x'), 'y', 'x');
        } else if (this.autoDimension === 'horizontal') {
            this.currentGoal.x = this._estimateHitPoint(this._findClosestBall('y'), 'x', 'y');
        }
    }

    _estimateHitPoint (ball, autoDimension, otherDimension) {
        if (ball && Math.abs(ball.velocity[autoDimension]) > 0) {
            // find out which x/y position ball will have
            let otherDimDiff = Math.abs(ball.position[otherDimension] - this.position[otherDimension]);
            let hitPoint = ball.position[autoDimension] + otherDimDiff * (ball.velocity[autoDimension] / ball.velocity[otherDimension]);
            return hitPoint;
        } else {
            return 0.5;
        }
    }

    /**
     * more lightweight method as key interaction not needed
     */
    onEvent (eventName) {
        if (eventName === 'pausing') {
            this.pause();
        } else if (eventName === 'unpausing') {
            this.unpause();
        }
    }
}