import { generateID } from '../Tools';

export class ColorTheme {

    constructor(name, colors, backgroundIndex = 0) {
        this.name = name;
        this.id = generateID();
        this.colors = [];
        for(let i = 0; i < colors.length; ++i) {
            this.colors.push('#' + colors[i]);
        }

        this.background = this.colors.splice(backgroundIndex, 1)[0];
        this.unique = this.copyColors();
    }

    getRandom() {
        return this.colors[Math.floor(Math.random() * this.colors.length)];
    }

    getRandomUnique() {
        if (this.unique.length < 1) {
            this.unique = this.copyColors();
        }

        let lottery = Math.floor(Math.random() * this.unique.length);
        return this.unique.splice(lottery, 1)[0];
    }

    get(i) {
        return this.colors[i % this.colors.length];
    }

    bg() {
        return this.background;
    }

    getLength() {
        return this.colors.length;
    }

    getColors() {
        return this.colors;
    }

    copyColors() {
        let copy = [];
        let i = this.colors.length;
        while (i--) copy[i] = this.colors[i];
        return copy;
    }

    copyAllColors() {
        let copy = [];
        let i = this.colors.length;
        while (i--) copy[i] = this.colors[i];
        copy.push(this.background);
        return copy;
    }
}