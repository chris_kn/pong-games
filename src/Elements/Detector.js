import autoBind from 'auto-bind';
import { generateID } from '../Tools';

export class Detector {

    constructor(engine, context, label, callback, centerX, centerY, width, height) {
        this.className = 'Detector';
        this.id = generateID();
        this.context = context;
        this.label = label;
        this.callback = callback;
        this.position = {
            x: centerX,
            y: centerY
        };
        this.width = width;
        this.height = height;

        autoBind(this);
    }

    draw() {
        // shall not be drawn, so leave it empty
    }
    
    translateX(percentage) {
        return Math.round(this.context.canvas.width * percentage);
    }

    translateY(percentage) {
        return Math.round(this.context.canvas.height * percentage);
    }

    onCollision(e) {
        this.callback(e);
    }
}