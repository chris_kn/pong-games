import { lottery } from '../Tools';
import autoBind from 'auto-bind';

export class Disease {
    constructor({ name , symptoms , heelingTime = 8 , contagiosity = 1 , callbacks}) {
        this.name = name;
        this.symptoms = symptoms;
        this.heelingTime = heelingTime;
        this.contagiosity = contagiosity;
        this.callbacks = callbacks;
        this.timeouts = {};
        autoBind(this);
    }

    expose(victim) {
        if (Math.random() <= this.contagiosity) {
            this.infect(victim);
        }
    }

    infect(victim) {
        // check if conditions are forbidding an infection
        if (typeof this.condition === 'function' && !this.condition(victim)) {
            return;
        }
        // disable previous diseases
        if (victim.disease !== undefined) {
            clearTimeout(victim.disease.timeouts[victim.id]);
            this.heel(victim, true);
        }
        victim.disease = this;
        // store original state and replace values with symptoms
        victim.originalValues = {};
        const keys = Object.keys(this.symptoms);
        for (let i = 0; i < keys.length; i++) {
            const key = keys[i];
            if (victim.hasOwnProperty(key)) {
                const value = this.symptoms[key];
                victim.originalValues[key] = victim[key];
                if (typeof value === 'function') {
                    victim[key] = value(victim);
                } else {
                    victim[key] = value;
                }
            }
        }

        if (typeof this.callbacks === 'object' && typeof this.callbacks.afterInfection === 'function') {
            this.callbacks.afterInfection(victim);
        }

        // clear timeouts of previous infections
        if (this.timeouts[victim.id]) {
            clearTimeout(this.timeouts[victim.id]);
        }
        
        this.timeouts[victim.id] = setTimeout(() => {
            if (typeof this.callbacks === 'object' && typeof this.callbacks.onHeelingTimeout === 'function') {
                this.callbacks.onHeelingTimeout(victim);
            } else {
                this.heel(victim);
            }
        }, this.heelingTime * 1000);
    }

    heel(victim, silent = false) {
        const keys = Object.keys(victim.originalValues);
        for (let i = 0; i < keys.length; i++) {
            victim[keys[i]] = victim.originalValues[keys[i]];
        }
        victim.disease = undefined;

        if (!silent && typeof this.callbacks === 'object' && typeof this.callbacks.onHeeled === 'function') {
            this.callbacks.onHeeled(victim);
        }
    }

    onCollision(otherObject) {
        if (!otherObject.isStatic) {
            if (lottery(this.contagiosity)) {
                if (typeof this.callbacks === 'object' && typeof this.callbacks.beforeInfection === 'function') {
                    this.callbacks.beforeInfection(otherObject);
                }
                this.expose(otherObject);
            }
        }
    }

    clearTimeouts() {
        for (let i = 0; i < this.timeouts.length; i++) {
            clearTimeout(this.timeouts[i]);
        }
    }
}