import { Disease } from './Disease';
import { addTransparency } from '../Tools';

export const diseaseCollection = [
    new Disease({
        name: 'shrinking',
        heelingTime: 10,
        contagiosity: 1,
        condition: function (victim) {
            return victim.className === 'Bar';
        },
        symptoms: {
            height: function (obj) {
                return obj.height * .5;
            },
            width: function (obj) {
                return obj.width * 0.5;
            }
        }
    }),
    new Disease({
        name: 'confusion',
        heelingTime: 15,
        contagiosity: 1,
        condition: function (victim) {
            return victim.className === 'Bar';
        },
        symptoms: {
            controls: function (obj) {
                return {
                    left: obj.originalValues.controls.right,
                    right: obj.originalValues.controls.left,
                    up: obj.originalValues.controls.down,
                    down: obj.originalValues.controls.up
                }
            }
        }
    }),
    new Disease({
        name: 'transparency',
        heelingTime: 8,
        contagiosity: 1,
        symptoms: {
            color: function (obj) {
                // get rgb values, add opacity
                return addTransparency(obj.color, .2);
            }
        }
    }),
    new Disease({
        name: 'slowness',
        heelingTime: 10,
        contagiosity: 1,
        condition: function (victim) {
            return victim.className === 'Bar';
        },
        symptoms: {
            speed: function (obj) {
                return {
                    x: obj.speed.x * .5,
                    y: obj.speed.y * .5
                }
            }
        }
    })
];

function getRandomDisease () {
    return diseaseCollection[Math.floor(Math.random() * diseaseCollection.length)];
}

function exposeToDisease ({ element , disease }) {
    if (Math.random() < disease.contagiosity) {
        disease.infect(element);
        return true;
    } else {
        return false;
    }
}

export function diseaseLottery (element, resistance) {
    resistance = resistance || element.resistance;
    if (Math.random() > resistance) {
        return exposeToDisease({
            element: element,
            disease: getRandomDisease()
        });
    } else {
        return false;
    }
}

export function clearTimeouts () {
    for (let i = 0; i < diseaseCollection.length; i++) {
        diseaseCollection[i].clearTimeouts();
    }
}