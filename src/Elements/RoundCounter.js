//import Matter from 'matter-js';
import { colorThemes } from '../ColorCollection';
import { lightenDarkenColor , generateID } from '../Tools';
import autoBind from 'auto-bind';
import WebFont from 'webfontloader';

export class RoundCounter {

    constructor(canvas, context, colorTheme, height) {
        this.className = 'RoundCounter';
        this.id = generateID();
        this.canvas = canvas;
        this.c = context;
        this.colorTheme = colorTheme || colorThemes[Math.abs(Math.random(colorThemes.length))];
        this.count = 0;
        this.shown = false;
        this.color = 'white';
        this.height = height || .2;

        WebFont.load({
            google: {
              families: ['Droid Sans', 'Droid Serif']
            }
        });

        autoBind(this);
    }

    flash() {
        this.color = this.colorTheme.getRandom();
        this.shown = true;
        setTimeout(()=>{
            this.shown = false;
        }, 500);
        setTimeout(()=>{
            this.shown = true;
        }, 1000);
        setTimeout(()=>{
            this.shown = false;
        }, 1500);
    }

    isShown() {
        return this.shown;
    }

    draw() {
        if (this.shown) {
            let height = Math.round(this.canvas.height * this.height);
            this.c.font = height + "px Droid Sans";
            this.c.fillStyle = lightenDarkenColor(this.color, 30);
            this.c.textBaseline = 'middle';
            this.c.strokeStyle = this.color;
            this.c.lineWidth = 2;
            this.c.textAlign = "center";
            this.c.fillText('Round ' + this.count + '!', 0.5 * this.canvas.width , 0.5 * this.canvas.height /* + .3 * this.height * this.canvas.height */);
            this.c.strokeText('Round ' + this.count + '!', 0.5 * this.canvas.width , 0.5 * this.canvas.height /* + .3 * this.height * this.canvas.height */);  
        }
    }

    incr() {
        this.count++;
    }

    reset() {
        this.count = 0;
        this.shown = false;
    }
}
