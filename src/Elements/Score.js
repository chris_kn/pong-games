import { lightenDarkenColor, generateID } from '../Tools';
import autoBind from 'auto-bind';
import WebFont from 'webfontloader';

export class Score {

    constructor(context, numberTeams = 2, color = '#7F7B7C', size = 0.1, font = 'Black Ops One', positionX = 0.5, positionY = 0.125) {
        this.className = 'TwoPlayerScore';
        this.id = generateID();
        this.canvas = context.canvas;
        this.context = context;
        this.numberTeams = numberTeams;
        this.color = color;
        this.size = size;
        this.positionX = positionX;
        this.positionY = positionY;
        this.shown = true;
        this.score = [];
        this.roundScore = [];
        this.scoreString = '';
        this.roundScoreString = '';

        for (let i = 0; i < this.numberTeams; ++i) {
            this.score.push(0);
            this.roundScore.push(0);
        }

        this.renderScore();
        this.renderRoundScore();

        WebFont.load({
            google: {
                families: ['Droid Sans', 'Droid Serif']
            }
        });

        autoBind(this);
    }

    renderScore() {
        let output = this.score[0];
        if (this.score.length > 1) {
            for (let i = 1; i < this.score.length; ++i) {
                output = output + ' : ' + this.score[i];
            }
        }
        this.scoreString = output;
    }

    renderRoundScore() {
        let output = this.roundScore[0];
        if (this.roundScore.length > 1) {
            for (let i = 1; i < this.roundScore.length; ++i) {
                output = output + ' : ' + this.roundScore[i];
            }
        }
        this.roundScoreString = output;
    }

    setScore(array) {
        if (array.isArray()) {
            this.score = array;
        } else {
            console.error('Score only accepts arrays!');
        }
    }

    setRoundscore(array) {
        if (array.isArray()) {
            this.roundScore = array;
        } else {
            console.error('RoundScore only accepts arrays!');
        }
    }

    resize() {

    }

    incr(index) {
        if (index < this.score.length && index > -1) {
            this.score[index]++;
            this.renderScore();
        }
    }

    decr(index) {
        if (index < this.score.length && index > -1) {
            this.score[index]--;
            this.renderScore();
        }
    }

    incrRound(index) {
        if (index < this.roundScore.length && index > -1) {
            this.roundScore[index]++;
            this.renderRoundScore();
        }
    }

    decrRound(index) {
        if (index < this.roundScore.length && index > -1) {
            this.roundScore[index]--;
            this.renderRoundScore();
        }
    }

    reset() {
        this.score = [];
        this.scoreString = '';

        for (let i = 0; i < this.numberTeams; ++i) {
            this.score.push(0);
        }

        this.renderScore();
    }

    resetRound() {
        this.score = [];
        this.roundScore = [];
        this.scoreString = '';
        this.roundScoreString = '';

        for (let i = 0; i < this.numberTeams; ++i) {
            this.score.push(0);
            this.roundScore.push(0);
        }

        this.renderScore();
        this.renderRoundScore();
    }

    show() {
        this.shown = true;
    }

    hide() {
        this.shown = false;
    }

    flash() {
        this.shown = false;
        setTimeout(() => {
            this.shown = true;
        }, 500);
        setTimeout(() => {
            this.shown = false;
        }, 1000);
        setTimeout(() => {
            this.shown = true;
        }, 1500);
    }

    flashQuick() {
        this.shown = false;
        setTimeout(() => {
            this.shown = true;
        }, 300);
        setTimeout(() => {
            this.shown = false;
        }, 700);
        setTimeout(() => {
            this.shown = true;
        }, 1000);
    }

    draw() {
        if (this.shown) {
            this.context.fillStyle = lightenDarkenColor(this.color, 30);
            this.context.strokeStyle = this.color;
            this.context.textAlign = "center";
            this.context.textBaseline = 'middle';
            this.context.font = Math.round(this.canvas.height * this.size * 0.5) + "px Droid Sans";
            this.context.fillText(this.roundScoreString, Math.round(this.canvas.width * this.positionX), Math.round(this.canvas.height * this.positionY - this.canvas.height * this.size * 0.7));
            this.context.strokeText(this.roundScoreString, Math.round(this.canvas.width * this.positionX), Math.round(this.canvas.height * this.positionY - this.canvas.height * this.size * 0.7));
            this.context.font = Math.round(this.canvas.height * this.size) + "px Droid Sans";
            this.context.fillText(this.scoreString, Math.round(this.canvas.width * this.positionX), Math.round(this.canvas.height * this.positionY));
            this.context.strokeText(this.scoreString, Math.round(this.canvas.width * this.positionX), Math.round(this.canvas.height * this.positionY));
        }
    }

    getLeading() {
        return Math.max(...this.score);
    }
}