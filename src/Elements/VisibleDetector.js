import { lightenDarkenColor , generateID } from '../Tools';
import { Detector } from './Detector';

export class VisibleDetector extends Detector {
    constructor(engine, label, callback, centerX, centerY, width, height, color) {
        super(engine, label, callback, centerX, centerY, width, height);
        this.fillColor = lightenDarkenColor(this.color, -30);
        this.strokeColor = lightenDarkenColor(this.color, -60);
    }

    draw() {
        this.vertices = this.element.vertices;
    
        this.c.beginPath();
        this.c.moveTo(this.vertices[0].x, this.vertices[0].y);

        for (var j = 1; j < this.vertices.length; j += 1) {
            this.c.lineTo(this.vertices[j].x, this.vertices[j].y);
        }

        this.c.lineTo(this.vertices[0].x, this.vertices[0].y);
        
        this.c.fillStyle = this.fillColor;
        this.c.strokeStyle = this.strokeColor;
        this.c.lineWidth = 1;
        this.c.fill();
        this.c.stroke();
    }
}