import { lightenDarkenColor , generateID } from '../Tools';
import autoBind from 'auto-bind';

export class Wall {

    /**
     * A static rectangle element that can be used to keep elements from moving out of a certain area
     * @param {object} context - The canvas' 2D context which is being created by PongGame and can be 
     * accessed by [myPongGame].c 
     * @param {number} x - x-position of the center of the Wall relative to the canvas' width,
     * usually between 0 and 1
     * @param {number} y - y-position of the center of the Wall relative to the canvas' height,
     * so usually between 0 and 1 if the element should appear on screen
     * @param {number} width - Width relative to canvas' width, must be bigger than 0. 
     * @param {number} height - Height relative to canvas' height, must be bigger than 0. 
     * @param {string} color - Color of the rectangle's border. The filling color will be the same color, 
     * but a bit brighter.
     */
    constructor(context, x, y, width, height, color) {
        this.className = 'Wall';
        this.isStatic = true;
        this.id = generateID();
        this.c = context;
        this.position = {
            x: x,
            y: y
        }
        this.velocity = {
            x: 0,
            y: 0
        }
        this.width = width;
        this.height = height;
        this.color = color;
        this.label = 'Wall';

        autoBind(this);
    }

    /**
     * draws the Wall
     */
    draw() {
        // ADAPT !!!

        this.c.beginPath();
        this.c.rect(this.getAbsX(this.leftBorder()), this.getAbsY(this.topBorder()), this.getAbsX(this.width), this.getAbsY(this.height)); 
        
        this.c.fillStyle = lightenDarkenColor(this.color, 40);
        this.c.strokeStyle = this.color;
        this.c.lineWidth = 1;
        this.c.fill();
        this.c.stroke();
    }

    /**
     * Returns the absolute pixel value for a number input that is reltive to the canvas' width
     * @param {number} relNumber - Some number relative to the canvas' width
     */
    getAbsX(relNumber) {
        return Math.round(this.c.canvas.width * relNumber);
    }

    /**
     * Returns the absolute pixel value for a number input that is reltive to the canvas' height
     * @param {number} relNumber - some numbe rrelative to the canvas' height
     */
    getAbsY(relNumber) {
        return Math.round(this.c.canvas.height * relNumber);
    }

    /**
     * Returns the position of the right border of the Wall, relative to the canvas' width
     */
    rightBorder() {
        return this.position.x + 0.5 * this.width;
    }

    /**
     * Returns the position of the left border of the Wall, relative to the canvas' width
     */
    leftBorder() {
        return this.position.x - 0.5 * this.width;
    }

    /**
     * Returns the position of the top border of the Wall, relative to the canvas' height
     */
    topBorder() {
        return this.position.y - 0.5 * this.height;
    }

    /**
     * Returns the position of the bottom border of the Wall, relative to the canvas' height
     */
    bottomBorder() {
        return this.position.y + 0.5 * this.height;
    }

    /**
     * Returns the hypothetical position and border positions (relative to the cavas size)
     * for the next frame. As the element is static, these values are identical with the current
     * position. The method is used for the collision detection.
     */
    getNextStep() {
        return {
            x: this.position.x,
            y: this.position.y,
            topBorder: this.topBorder(),
            bottomBorder: this.bottomBorder(),
            leftBorder: this.leftBorder(),
            rightBorder: this.rightBorder()
        }
    }
    
    /**
     * Returns the hypothetical position and border positions in pixels for the next frame. As the element
     * is static, these values are identical with the current position. The method is used for
     * the collision detection. 
     */
    getNextStepAbs() {
        return {
            width: this.getAbsX(this.width),
            height: this.getAbsY(this.height),
            x: this.getAbsX(this.position.x + this.velocity.x),
            y: this.getAbsY(this.position.y + this.velocity.y),
            topBorder: this.getAbsY(this.topBorder() + this.velocity.y),
            bottomBorder: this.getAbsY(this.bottomBorder() + this.velocity.y),
            leftBorder: this.getAbsX(this.leftBorder() + this.velocity.x),
            rightBorder: this.getAbsX(this.rightBorder() + this.velocity.x)
        }
    }    
}