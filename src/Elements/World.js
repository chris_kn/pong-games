import { generateID } from '../Tools';
import { diseaseLottery , clearTimeouts } from '../Elements/DiseaseCollection';

export class World {

    /**
     * Object contaning all the elements, triggering collision detecions and position updates
     * for all elements it contains. Create an instance of this and add Elements with the provided
     * method addElement.
     * @param {number} gravity - A number setting the amount that is added to the objects velocity
     * at every update 
     * @param {function} collisionCallback - Optional callback for when a ball hits another Element.
     * Can be used e.g. to accelerate the ball on certain conditions 
     */
    constructor({ gravity , useDiseases = false , friction }) {
        this.id = generateID();
        this.gravity = gravity;
        this.friction = friction;
        this.elements = [];
        this.balls = [];
        this.players = [];
        this.walls = [];
        this.useDiseases = useDiseases;
        this.collisionCallbacks = [];
        this.frameStep = 1 / 60 * 1000;
        if (this.useDiseases) {
            this.collisionCallbacks.push(this._initInfections);
        }
    }

    /**
     * Clears all of the World's arrays
     */
    clear() {
        clearTimeouts();
        this.collisionCallback = null;
        this.elements = [];
        this.balls = [];
        this.players = [];
        this.walls = [];
    }

    /**
     * Adds the element to the world's elements array, as well as to the respective array depending
     * on the type of the object. (All Walls, Balls and Bars are referrenced in their respective array.)
     * RoundCounters are added at the end of the array to make sure they are drawn on top of the other
     * elements.
     * @param {object} element 
     */
    addElement(element) {
        if (typeof element === 'object') {
            if (element.className === 'Ball') {
                this.balls.push(element);
                if (this.useDiseases) {
                    diseaseLottery(element, .3);
                }
                element.setCollisionCallback(this._onCollision.bind(this));
            } else if (element.className === 'Bar' || element.className === 'Player') {
                this.players.push(element);
            } else if (element.className === 'Wall') {
                this.walls.push(element);
            }

            let index = this.elements.length;

            if (element.className.toLowerCase().indexOf('roundcounter') === -1) {
                for (let i = this.elements.length - 1; i > -1; --i) {
                    if (this.elements[i].className.toLowerCase().indexOf('roundcounter') > -1) {
                        index--;
                    }
                }
            }

            this.elements.splice(index, 0, element);
        }
    }

    /**
     * Removes the elemets with the given id. For Balls, Walls and Brs, the element'S
     * reference is also removed from the respective array
     * @param {string} id - Element's id which is automatically generated in the Element's constructor
     * and accessible via "[elementname].id"
     */
    removeElement(id) {
        if (id !== undefined) {

            let className = null;
            let removedElement = null;

            for (let i = 0; i < this.elements.length; ++i) {
                if (this.elements[i].id === id) {
                    className = this.elements[i].className;
                    removedElement = this.elements.splice(i, 1);
                    break;
                }
            }

            if (className === 'Ball') {
                for (let j = 0; j < this.balls.length; ++j) {
                    if (this.balls[j].id === id) {
                        this.balls.splice(j, 1);
                        break;
                    }
                }
            } else if (className === 'Bar' || className === 'Player') {
                for (let j = 0; j < this.players.length; ++j) {
                    if (this.players[j].id === id) {
                        this.players.splice(j, 1);
                        break;
                    }
                }
            } else if (className === 'Wall') {
                for (let j = 0; j < this.walls.length; ++j) {
                    if (this.walls[j].id === id) {
                        this.walls.splice(j, 1);
                        break;
                    }
                }
            }

            return removedElement;

        } else {
            return null;
        }
    }

    /**
     * Method to update element positions. Checks for collisions first. Call this method once
     * on every frame cycle
     */
    update() {
        // first resolve collision conflicts
        this._checkCollisions();
        // then update element positions
        this._update();
    }

    /**
     * Draws all elements. Call this method once (before or after the updating element positions) on
     * every frame cycle
     */
    draw() {
        for (let i = 0; i < this.elements.length; ++i) {
            this.elements[i].draw();
        }
    }

    /**
     * Returns all players in this world with the given teamIndex
     * @param {number} teamIndex 
     */
    getTeamMembers(teamIndex) {
        let members = [];
        for (let i = 0; i < this.players.length; i++) {
            if (this.players[i].teamIndex === teamIndex) {
                members.push(this.players[i]);
            }
        }
        return members;
    }

    addCollisionCallback (cb) {
        if (typeof cb === 'function') {
            this.collisionCallbacks.push(cb);
        }
    }

    _onCollision (ball, rect) {
        for (let i = 0; i < this.collisionCallbacks.length; i++) {
            this.collisionCallbacks[i](ball, rect);
        }
    }

    /**
     * Internal function that loops through moving elements and initialtes a collision check
     */
    _checkCollisions() {
        // check player collisions
        for (let p = 0, len = this.players.length; p < len; ++p) {
            this.players[p].checkCollisions();
        }
        // check ball collisions
        for (let b = 0, len = this.balls.length; b < len; ++b) {
            this.balls[b].checkCollisions();
        }
    }

    /**
     * Internal function that loops through moving elements and initiates position updates for
     * moving elements
     */
    _update() {
        for (let p = 0, len = this.players.length; p < len; ++p) {
            this.players[p].update();
        }
        // check ball collisions
        for (let b = 0, len = this.balls.length; b < len; ++b) {
            this.balls[b].update();
        }
    }

    _initInfections(ball, rect) {
        if (ball.disease !== undefined && !rect.isStatic) {
            ball.disease.onCollision(rect);
        } else if (rect.disease !== undefined) {
            rect.disease.onCollision(ball);
        }
    }
}