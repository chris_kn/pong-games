import WebFont from 'webfontloader';
import autoBind from 'auto-bind';

export class Writing {
    constructor(context, words, x, y, size, font = 'Droid Sans', color = '#787878', strokeColor) {
        this.className = 'Writing';
        this.c = context;
        this.words = words;
        this.position = {
            x: x,
            y: y
        };
        this.size = size;
        this.font = font;
        this.color = color;
        this.shown = true;

        if (typeof strokeColor !== 'string') {
            this.hasStroke = false;
        } else {
            this.hasStroke = true;
            this.strokeColor = strokeColor;
        }

        WebFont.load({
            google: {
                families: [this.font]
            }
        });

        autoBind(this);
    }

    draw() {
        if (this.shown) {
            let height = Math.round(this.c.canvas.height * this.size);
            this.c.font = height + "px Droid Sans";
            this.c.fillStyle = this.color;
            this.c.textBaseline = 'middle';
            
            this.c.textAlign = "center";
            this.c.fillText(this.words, this.position.x * this.c.canvas.width, this.position.y * this.c.canvas.height);
            if (this.hasStroke) {
                this.c.strokeStyle = this.strokeColor;
                this.c.lineWidth = 2;
                this.c.strokeText(this.words, this.position.x * this.c.canvas.width, this.position.y * this.c.canvas.height);
            }
        }
    }

    hide() {
        this.shown = false;
    }

    show() {
        this.shown = true;
    }

    setSize(newSize) {
        if (typeof newSize === 'number') {
            this.size = newSize;
        }
    }

    setPosition(newPosition) {
        if (typeof newPosition === 'object' && newPosition.x && newPosition.y) {
            this.position = {
                x: newPosition.x,
                y: newPosition.y
            }
        } else {
            console.error('"setPosition" expects an object containing an x and y value!');
        }
    }
}