import { Pong1 } from './Pong1';
import get from '../Translations';
import iconPong1 from '../img/pong1.svg';
import iconPong2 from '../img/pong2.svg';
import iconPong3 from '../img/pong3.svg';
import iconPong4 from '../img/pong4.svg';
import iconArrowUp from '../img/arrow-up.svg';
import iconArrowDown from '../img/arrow-down.svg';
import iconArrowLeft from '../img/arrow-left.svg';
import iconArrowRight from '../img/arrow-right.svg';

let language = 'en';

export const GameCollection = [{
    game: Pong1,
    id: 'pong1',
    icon: iconPong1,
    description: {
        title: 'Pong 1',
        minPlayers: 2,
        maxPlayers: 4,
        controls: {
            up: {
                icon: iconArrowUp,
                name: 'Move Up'
            },
            down: {
                icon: iconArrowDown,
                name: 'Move Down'
            }
        }
    },
    options: {
        players: [{
                name: 'Player 1',
                controls: {
                    up: 'ArrowUp',
                    down: 'ArrowDown'
                },
                activated: true,
                useKeyboard: false,
                autopilot: false
            },
            {
                name: 'Player 2',
                controls: {
                    up: 'e',
                    down: 'd'
                },
                activated: true,
                useKeyboard: false,
                autopilot: false
            },
            {
                name: 'Player 3',
                controls: {
                    up: 'h',
                    down: 'b'
                },
                activated: false,
                useKeyboard: false,
                autopilot: false
            },
            {
                name: 'Player 4',
                controls: {
                    up: 'p',
                    down: 'l'
                },
                activated: false,
                useKeyboard: false,
                autopilot: false
            }
        ],
        colorTheme: undefined

    }
}, {
    game: null,
    id: 'pong2',
    notPublished: true,
    icon: iconPong2,
    description: {
        Title: 'Pong 2',
        minPlayers: 2,
        maxPlayers: 4,
        controls: {
            left: {
                icon: iconArrowLeft,
                name: 'Move Left'
            },
            right: {
                icon: iconArrowRight,
                name: 'Move Right'
            }
        }
    }
}, {
    game: null,
    id: 'pong3',
    notPublished: true,
    icon: iconPong3,
    description: {
        Title: 'Pong 3',
        minPlayers: 2,
        maxPlayers: 2,
        controls: {
            up: {
                icon: iconArrowUp,
                name: 'Move Up'
            },
            down: {
                icon: iconArrowDown,
                name: 'Move Down'
            }
        }
    }
}, {
    game: null,
    id: 'pong4',
    notPublished: true,
    icon: iconPong4,
    description: {
        Title: 'Pong 4',
        minPlayers: 2,
        maxPlayers: 2,
        controls: {
            up: {
                icon: iconArrowLeft,
                name: 'Move Left'
            },
            down: {
                icon: iconArrowRight,
                name: 'Move Right'
            }
        }
    }
}];

export function getGameDescription(id) {
    for (let i = 0; i < GameCollection.length; ++i) {
        if (GameCollection[i].id === id) {
            return GameCollection[i].description;
        }
    }
};

export function getGame(id) {
    for (let i = 0; i < GameCollection.length; ++i) {
        if (GameCollection[i].id === id) {
            return GameCollection[i].game;
        }
    }
};

export function getDefaultOptions(id) {
    for (let i = 0; i < GameCollection.length; ++i) {
        if (GameCollection[i].id === id) {
            return GameCollection[i].options;
        }
    }
}

export function getLanguage () {
    return language;
}