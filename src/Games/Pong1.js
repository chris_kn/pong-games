import { PongGame } from './PongGame';
import { Ball } from '../Elements/Ball';
import { Bar } from '../Elements/Bar';
import { BarAutoPilot } from '../Elements/BarAutoPilot';
import { Wall } from '../Elements/Wall';
import { RoundCounter } from '../Elements/RoundCounter';
import { Score } from '../Elements/Score';
import { getIndexOfMax } from '../Tools';
import autoBind from 'auto-bind';

export class Pong1 extends PongGame {

    // --------------- FUNCTIONS -----------------------
    constructor(containerId, options, callbacks) {
        options.useDiseases = true;
        options.friction = .03;
        super(containerId, options, callbacks);
        this.callbacks.onBallsOffScreen = this.onBallsOffScreen;

        this.grey1 = '#7F7B7C';
        this.NUMBER_POINTS_ROUND = 0;
        this.NUMBER_ROUNDS_GAME = 5;
        autoBind(this);
    }

    setup() {

        super.setup();

        this.ballRadius = 0.02;
        this.ballsOffCount = 0;
        this.barStartXLeft = 0.125;
        this.barStartXRight = 0.875;

        this.roundCount = new RoundCounter(this.canvas, this.c, this.colorTheme);
        this.addElement(this.roundCount);
        this.score = new Score(this.c, 2, this.grey1, 0.1, 'Black Ops One', 0.5, 0.125);
        this.addElement(this.score);

        // add walls
        let wallThickness = 0.025;
        this.addElement(new Wall(this.c, 0.5, wallThickness / 2, 1, wallThickness, this.grey1));
        this.addElement(new Wall(this.c, 0.5, 1 - (wallThickness / 2), 1, wallThickness, this.grey1));

        this.addPlayers();
        this.canvasResize();
        this.startGame();
    }

    addPlayers() {
        const players = this.options.players;
        // add player bars
        let count = 0;
        for (let p = 0; p < players.length; ++p) {
            if (players[p].activated === true || players[p].activated === 'on') {
                if (count % 2 === 1) {
                    if (players[p].autopilot) {
                        this.addRegisteredElement(new BarAutoPilot({
                            world: this.world, 
                            context: this.c, 
                            playerIndex: count,
                            teamIndex: count % 2,
                            options: players[p], 
                            x: this.barStartXRight - 0.125 * 0.5 * count, 
                            y: 0.5, 
                            width: 0.015, 
                            height: 0.1, 
                            color: this.colorTheme.getRandomUnique(),
                            autoDimension: 'vertical',
                            delay: 1
                        }));
                    } else {
                        this.addRegisteredElement(new Bar({
                            world: this.world, 
                            context: this.c, 
                            playerIndex: count,
                            teamIndex: count % 2,
                            options: players[p], 
                            x: this.barStartXRight - 0.125 * 0.5 * count, 
                            y: 0.5, 
                            width: 0.015, 
                            height: 0.1, 
                            color: this.colorTheme.getRandomUnique()
                        }));                    }
                } else {
                    this.addRegisteredElement(new Bar({
                        world: this.world,
                        context: this.c, 
                        playerIndex: count, 
                        teamIndex: count % 2,
                        options: players[p], 
                        x: this.barStartXLeft + 0.125 * 0.5 * (count - 1), 
                        y: 0.5, 
                        width: 0.015, 
                        height: 0.1, 
                        color: this.colorTheme.getRandomUnique()
                    }));
                }
                count++;
            }

        }
    }

    addBalls(numBalls, ballRadius) {
        const minSpeedX = 0.001;
        let ballMargin = this._calcBallMargin(ballRadius) * 3;
        if (numBalls % 2 === 0) {
            for (let b = 0; b < numBalls; ++b) {
                this.addElement(new Ball({
                    world: this.world,
                    c: this.c,
                    x: 0.5, 
                    y: 0.5 + ballMargin * (b - numBalls / 2), 
                    radius: ballRadius, 
                    color: this.colorTheme.getRandomUnique(), 
                    minSpeed: {
                        x: minSpeedX,
                        y: 0
                    },
                    speedAugmentation: 0.03
                }));
            }
        } else {
            for (let b = 0; b < numBalls; ++b) {
                this.addElement(new Ball({ 
                    world: this.world,
                    c: this.c,
                    x: 0.5, 
                    y: 0.5 + ballMargin * (b - ((numBalls - 1) / 2)), 
                    radius: ballRadius, 
                    color: this.colorTheme.getRandomUnique(),
                    minSpeed: {
                        x: minSpeedX,
                        y: 0
                    }
                }));
            }
        }
    }

    startGame() {

        this.numBalls = 1;
        this.roundCount.reset();

        this.startRender();
        this.startRound();

    }

    startRound() {
        if (this.roundCount.count === this.NUMBER_ROUNDS_GAME) {
        // if (this.roundCount.count >= 1) { // line is for debugging only
            this.gameOver();
            return;
        }
        this.roundCount.incr();
        this.score.reset();
        this.numBalls = this.roundCount.count;
        this.NUMBER_POINTS_ROUND = this.numBalls * 5;
        this.kickoff(2);
        this.roundCount.flash();
    }

    startKickOff() {
        this.score.flashQuick();
        this.kickoff(2);
    }

    kickoff(delay) {
        this.ballsOffCount = 0;

        this.addBalls(this.numBalls, this.ballRadius);
        this.pausing = false;

        const that = this;
        
        for (let e = 0; e < that.world.balls.length; ++e) {
            that.world.balls[e].kickoff(0.006, delay);
        }
    }

    onBallsOffScreen(offScreenBalls) {
        for (let i = 0; i < offScreenBalls.length; ++i) {
            if (offScreenBalls[i].position.indexOf('left') !== -1) {
                this.score.incr(1);
                this.removeElement(offScreenBalls[i].element.id);
            } else if (offScreenBalls[i].position.indexOf('right') !== -1) {
                this.score.incr(0);
                this.removeElement(offScreenBalls[i].element.id);
            }
        }

        if (this.world.balls.length <= 0) {
            if (this.score.getLeading() >= this.NUMBER_POINTS_ROUND) {
                // increment round score of this round's winning team
                let winners = getIndexOfMax(this.score.score);

                for (let i = 0; i < winners.length; ++i) {
                    this.score.incrRound(winners[i]);
                }
                
                this.startRound();
            } else {
                this.startKickOff();
            }
        }
    }

    gameOver() {

        // stop animation frame and remove listeners
        super.gameOver();

        // get winning team
        const winIndex = getIndexOfMax(this.score.roundScore);
        // get winners' and losers' names
        const players = this.world.players;
        let winners = [];
        let losers = [];

        // add all members of the winning team(s)
        for (let i = 0; i < winIndex.length; i++) {
            winners = winners.concat(this.world.getTeamMembers(winIndex[i]));
        }

        // now see who is not a winner and call them losers
        for (let k = 0; k < players.length; k++) {
            let isAWinner = false;
            let l = winners.length - 1;
            while(l > -1 && isAWinner === false) {
                if (players[k].id === winners[l].id) {
                    isAWinner = true;
                }
                --l;
            }
            if (!isAWinner) {
                losers.push(players[k]);
            }
        }

        this.callbacks.onGameOver(winners, losers);
    }

    onCollision(ball, rect) {
        if (rect.className === 'Bar') {
            ball.changeSpeed(1.1);
        }
        super.onCollision(ball, rect);
    }

    _calcBallMargin(radius) {
        return radius * (this._getCanvasDiagonal() / this.canvas.height);
    }

    _getCanvasDiagonal() {
        return Math.sqrt(Math.pow(this.canvas.width, 2) + Math.pow(this.canvas.clientHeight, 2));
    }
}