import { World } from '../Elements/World';
import { getTheme, getRandomTheme } from '../ColorCollection';

import autoBind from 'auto-bind';

export class PongGame {

    /**
     * The base class for all the pong games containing the setup and the values that all games
     * need. Pass the id of the canvas element, that you want the game to be rendered in.
     * @param {string} canvasId - Id of the canvas DOM element that the game will be rendered in
     * @param {object} options - Object that can contain the following options:
     * @param {ColorTheme} options.colorTheme - ColorTheme used to paint all the elements of the game (see ColorTheme class
     * for more information)
     * @param {number} options.gravity - A number specifying the value added to the balls' y-velocity at
     * every cycle
     * @param {object} callbacks 
     */
    constructor(canvasId, options = {}, callbacks) {
        this.canvasId = canvasId;
        this.colorTheme = options.colorTheme !== undefined ? getTheme(options.colorTheme) : getRandomTheme();
        this.options = options || undefined;
        this.gravity = options.gravity || 0;
        this.friction = options.friction;
        this.listeners = [];
        this.callbacks = callbacks || {};
        this.callBackLoop = {
            active: true,
            intervall: 10,
            count: 0
            /* ,
                        callback: function(){} */
        };
        this.useDiseases = options.useDiseases === true;
        this.allowTogglePause = true;
        this.nonGameKeys = ['F5', 'F12'];
        this.pauseKeys = [' ', 'Escape', 'Pause'];
        this.c = null;
        this.resizeHandler = this.canvasResize.bind(this);
        this.keyUpHandler = this.handleKeyUp.bind(this);
        this.keyDownHandler = this.handleKeyDown.bind(this);
        autoBind(this);
    }

    addElement(newElement) {
        this.world.addElement(newElement);
    }

    addRegisteredElement(newElement) {
        this.world.addElement(newElement);
        this.registerListener(newElement);
    }

    removeElement(elementId) {
        this.world.removeElement(elementId);
    }

    deleteRegisteredElement(elementId) {
        for (let i = 0; i < this.listeners.length; i++) {
            if (this.world.elements[i].id === elementId) {
                this.listeners.splice(i, 1);
            }
        }
        for (let i = 0; i < this.world.elements.length; i++) {
            if (this.world.elements[i].id === elementId) {
                this.world.elements.splice(i, 1);
            }
        }
    }

    setup() {

        this.canvas = document.getElementById(this.canvasId);
        this.processOptions(this.options);

        this.world = new World({
            gravity: this.gravity,
            friction: this.friction,
            useDiseases: this.useDiseases
        });

        this.keysdown = [];
        this.c = this.canvas.getContext("2d");
        this.canvas.style.background = this.colorTheme.bg();
        this.isPausing = false;

        this.canvasResize();
        this.addStandardListeners();
    }

    processOptions(options) {

        this.options = options || {};

        const defaultPlayerSetup = {
            player1: {
                class: 'Player',
                name: 'Player 1',
                controls: {
                    up: 'ArrowUp',
                    down: 'ArrowDown'
                }
            },
            player2: {
                class: 'Player',
                name: 'Player 2',
                controls: {
                    up: 'q',
                    down: 'a'
                }
            }
        }

        this.players = options.players || defaultPlayerSetup;
        this.ballRadius = options.ballRadius || this.canvas.width / 35;
        this.numPlayers = options.numPlayers || 2;
        this.sensitivity = options.sensitivity || [8, 8, 8, 8];

    }

    canvasResize() {
        let container = document.getElementById(this.canvasId).parentNode;
        //this.canvas = container.getElementsByTagName('canvas')[0];
        this.c.canvas.width = container.clientWidth;
        this.c.canvas.height = container.clientHeight;
    }

    addStandardListeners() {
        window.addEventListener('resize', this.resizeHandler);
        window.addEventListener('keydown', this.keyDownHandler);
        window.addEventListener('keyup', this.keyUpHandler);
    }

    removeStandardListeners() {
        window.removeEventListener('resize', this.resizeHandler);
        window.removeEventListener('keydown', this.keyDownHandler);
        window.removeEventListener('keyup', this.keyUpHandler);
    }

    handleKeyDown(e) {

        if (this.nonGameKeys.indexOf(e.key) !== -1) return;

        if (this.pauseKeys.indexOf(e.key) !== -1 && this.allowTogglePause === true) {
            if (this.isPausing === true) {
                if (this.callbacks.onUnPause !== undefined) {
                    this.callbacks.onUnPause();
                }
                this.unpause();

            } else {
                this.pause();
                if (this.callbacks.onPause !== undefined) {
                    this.callbacks.onPause();
                }
            }
            this.allowTogglePause = false;

        } else {
            for (let l = 0; l < this.listeners.length; ++l) {
                this.listeners[l].onEvent('keydown', e);
            }
        }

        e.preventDefault();
    }

    handleKeyUp(e) {

        if (this.nonGameKeys.indexOf(e.key) !== -1) return;

        if (this.pauseKeys.indexOf(e.key) !== -1) {
            this.allowTogglePause = true;
        } else {
            for (let l = 0; l < this.listeners.length; ++l) {
                this.listeners[l].onEvent('keyup', e);
            }
        }

        e.preventDefault();
    }

    startRender() {
        //Matter.Engine.run(this.engine);
        let oldTime = Date.now();
        this.render(oldTime);
    }

    render(oldTime) {

        let newTime = Date.now();
        let delta = newTime - oldTime;

        if (delta >= this.world.frameStep) {

            this.c.clearRect(0, 0, this.canvas.width, this.canvas.height);

            if (!this.isPausing) {
                this.world.update();
            }

            this.world.draw();

            oldTime = newTime - (delta % this.world.frameStep);

            if (this.callBackLoop.active) {
                if (this.callBackLoop.count === this.callBackLoop.intervall) {
                    this.callBackLoop.count = 0;
                    this._checkBallsOffScreen();
                    /* this.callBackLoop.callback(); */
                } else {
                    this.callBackLoop.count++;
                }
            }
        }

        if(!this.isPausing) {
            this.frameId = window.requestAnimationFrame(this.render.bind(this), oldTime);
        }
    }

    registerListener(element) {
        this.listeners.push(element)
    }

    addCallbackLoop(intervall, callback) {
        this.callBackLoop = {
            active: true,
            intervall: intervall,
            count: 0,
            callback: callback
        };
    }

    pause() {
        this.isPausing = true;
        window.cancelAnimationFrame(this.frameId);
    }

    unpause() {
        this.isPausing = false;
        let oldTime = Date.now();
        this.render(oldTime);
    }

    clear() {
        this.world.clear();
        this.removeStandardListeners();
        window.cancelAnimationFrame(this.frameId);
    }

    gameOver() {
        this.isPausing = true;
        this.removeStandardListeners();
        window.cancelAnimationFrame(this.frameId);
    }

    _checkBallsOffScreen() {
        let offScreenBalls = [];
        for (let i = 0; i < this.world.balls.length; ++i) {
            const ball = this.world.balls[i];
            let offScreenPosition = [];
            if (ball.position.x < 0) {
                offScreenPosition.push('left');
            } else if (ball.position.x > 1) {
                offScreenPosition.push('right');
            }

            if (ball.position.y < 0) {
                offScreenPosition.push('top');
            } else if (ball.position.y > 1) {
                offScreenPosition.push('bottom');
            }

            if (offScreenPosition.length > 0) {
                offScreenBalls.push({
                    element: ball,
                    position: offScreenPosition
                })
            }
        }
        if (offScreenBalls.length > 0 && this.callbacks.onBallsOffScreen !== undefined) {
            this.callbacks.onBallsOffScreen(offScreenBalls);
        }
    }

    getNumberBalls() {
        return this.world.balls.length;
    }
}