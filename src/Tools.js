export function lightenDarkenColor(col, amt) {
    if (col[0] === '#') {
        return lightenDarkenHex(col, amt);
    } else if (col.indexOf('rgba(') === 0) {
        return lightenDarkenRGBA(col, amt);
    } else if (col === 'black') {
        return lightenDarkenHex('#000000', amt);
    }
}

function lightenDarkenRGBA (col, amt) {
    let substring = col.split(/[,()]/);
    var r = parseInt(substring[1], 10) + amt;
    var g = parseInt(substring[2], 10) + amt;
    var b = parseInt(substring[3], 10) + amt;
    var a = parseFloat(substring[4], 10);
    return 'rgba(' + r + ',' + g + ',' + b + ',' + a + ')';
}

function lightenDarkenHex (col, amt) {

    var usePound = true;

    if (col[0] === "#") {
        col = col.slice(1);
        usePound = true;
    }

    var num = parseInt(col, 16);

    var r = (num >> 16) + amt;

    if (r > 255) r = 255;
    else if (r < 0) r = 0;

    var b = ((num >> 8) & 0x00FF) + amt;

    if (b > 255) b = 255;
    else if (b < 0) b = 0;

    var g = (num & 0x0000FF) + amt;

    if (g > 255) g = 255;
    else if (g < 0) g = 0;

    return (usePound ? "#" : "") + (g | (b << 8) | (r << 16)).toString(16);
}

/**
 * Simple is object check.
 * @param item
 * @returns {boolean}
 */
export function isObject(item) {
    return (item && typeof item === 'object' && !Array.isArray(item) && item !== null);
}

/**
 * Deep merge two objects.
 * @param target
 * @param source
 */
export function mergeDeep(target, source) {
    if (isObject(target) && isObject(source)) {
        Object.keys(source).forEach(key => {
            if (isObject(source[key])) {
                if (!target[key]) Object.assign(target, {
                    [key]: {}
                });
                mergeDeep(target[key], source[key]);
            } else {
                Object.assign(target, {
                    [key]: source[key]
                });
            }
        });
    }
    return target;
}

function r4() {
    return Math.random().toString(16).slice(-4);
}

export function generateID() {
    const ID = r4() + r4() + '-' + r4() + '-' + r4() + '-' + r4() + r4() + r4();
    return ID;
}

export function getIndexOfMax(array) {
    if (array.length < 1) return -1;
    let currMax = array[0];
    let maxIndex = [];
    let foundNewMax = true;

    while (foundNewMax) {
        foundNewMax = false;
        for (let i = 0; i < array.length; ++i) {
            if (array[i] > currMax) {
                currMax = array[i];
                maxIndex = [i];
                foundNewMax = true
            } else if (array[i] === currMax && maxIndex.indexOf(i) === -1) {
                maxIndex.push(i);
            }
        }
    }

    return maxIndex;
}

export function getIndexOfMin(array) {
    if (array.length < 1) return -1;
    let currMin = array[0];
    let minIndex = [];
    let foundNewMin = true;

    while (foundNewMin) {
        for (let i = 0; i < array.length; ++i) {
            if (array[i] > currMin) {
                currMin = array[i];
                minIndex = [i];
                foundNewMin = true;
            } else if (array[i] === currMin && minIndex.indexOf(i) === -1) {
                minIndex.push(i);
            }
        }
    }

    return minIndex;
}

/**
 * Randomly returns true or false depending on the handed chances.
 * @param {Number} chances - floating point number between 0 and 1, 1 representing 100% chances 
 */
export function lottery(chances) {
    const draw = Math.random();
    if (draw <= chances) {
        return true;
    } else {
        return false;
    }
}

export function addTransparency(hexColor, opacity = .5) {
    const color = hexToRgb(hexColor);
    return 'rgba(' + color.r + ',' + color.g + ',' + color.b + ',' + opacity + ')';
    // return 'rgba(${color.r}, ${color.g}, ${color.b}, ${opacity})';
}

function hexToRgb(hex) {
    hex = hex.toLowerCase();
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}