const Translations = {
    'en': {
        name: 'name',
        useKeyboard: 'use keyboard',
        player: 'player',
        autopilot: 'autopilot',
        moveUp: 'move up',
        moveDown: 'move down',
        moveRight: 'move right',
        moveLeft: 'move left'
    }
}

export default function get (language, word) {
    if (Translations.hasOwnProperty(language) && Translations[language].hasOwnProperty(word)) {
        return Translations[language][word];
    } else {
        return new Error('Translation not defined!');
    }
}