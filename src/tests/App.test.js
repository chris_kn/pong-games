import React from 'react';
import ReactDOM from 'react-dom';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, mount, render } from 'enzyme';
import App from '../App';
import { mockCanvas } from './CanvasMock';

configure({ adapter: new Adapter() });
const window = document.defaultView;
mockCanvas(window);

it('App renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
});

it('Test PongGame1 settings', () => {
    const wrapper = mount(<App />);
    // click on first game icon to show settings of PongGame1
    const icon = wrapper.find('.game-icon').first();
    icon.simulate('click');
    expect(wrapper.find('SettingsPage').length).toBeGreaterThan(0);

    // store settings for player 3 ()
    const player3Settings = wrapper.find('PlayerSettings').at(2);

    // click on first toggle button to enable the player
    player3Settings.find('ToggleButton').first().simulate('click');
    // click on second toggle button to disable on-screen controls
    player3Settings.find('ToggleButton').at(1).simulate('click');

    // change the playeres name
    const input = player3Settings.find('TextInput').first();
    input.simulate('focus');
    input.simulate('change', { target: { value: 'TestName' } });

    // set the x button as control
    const button = player3Settings.find('SetControlInput').first();
    button.simulate('click');
    button.simulate('keydown', { key: 'x' });

    // change color theme
    wrapper.find('ColorThemePicker .color-picker-next-button').first().simulate('click');

    // check if all settings were correctly applied
    const currSettings = wrapper.instance().options.pong1;
    // are there options for the game?
    expect(currSettings).not.toBeUndefined();
    // has player 3 been enabled?
    expect(currSettings.players[2].enabled).toBe(true);
    // have the on-screen controls been deactivated?
    expect(currSettings.players[2].useOnScreenControls).toBe(false);
    // has the name been saved?
    expect(currSettings.players[2].name).toBe('TestName');
    // has the control key been adapted?
    expect(currSettings.players[2].controls.up).toBe('x');

});

it('Test PongGame1 game run', () => {
    const div = document.createElement('div');
    document.body.appendChild(div);
    const wrapper = mount(<App />, { attachTo: div });
    const icon = wrapper.find('.game-icon').first();
    // click on game icon to display settings
    icon.simulate('click');
    // don't change anything, click on gam icon again to start game directly
    icon.simulate('click');

    //game should be running
    expect(wrapper.find('GameContainer').to)

    // click on all controls
    const controls = wrapper.find('.control-button');
    for (let i = 0; i < controls.length; i++) {
        controls.at(i).simulate('click');
    }

});